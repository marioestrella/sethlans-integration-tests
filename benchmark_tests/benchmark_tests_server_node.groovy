stage('Benchmark Test Server and Nodes - CPU') {
    String current_test = "test -Dtest=BenchmarkTestsServerNodeCPU"
    env.STAGENAME = "benchmark_test_server_node_cpu"

    parallel server: {
        node('server && linux') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def node_startup = load "${rootDir}/scripts/node_startup.groovy"
            def SERVERHOSTNAME = node_startup.linux_startup_script()

            script {
                env.SERVERNODE = env.NODE_NAME
                env.SERVERHOST = SERVERHOSTNAME
            }
        }
    } , node1: {
        node('node && windows') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def node_startup = load "${rootDir}/scripts/node_startup.groovy"
            def NODE1HOSTNAME = node_startup.windows_startup_script()
            script {
                env.NODENODE1 = env.NODE_NAME
                env.NODEHOST1 = NODE1HOSTNAME
            }

        }
    }, node2: {
        node('node && mac') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def node_startup = load "${rootDir}/scripts/node_startup.groovy"
            def NODE2HOSTNAME = node_startup.mac_startup_script()
            script {
                env.NODENODE2 = env.NODE_NAME
                env.NODEHOST2 = NODE2HOSTNAME
            }

        }
    }, node3: {
        node('node && linux') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def node_startup = load "${rootDir}/scripts/node_startup.groovy"
            def NODE3HOSTNAME = node_startup.linux_startup_script()
            script {
                env.NODENODE3 = env.NODE_NAME
                env.NODEHOST3 = NODE3HOSTNAME
            }
        }
    }


    node('selenium && dedicated') {
        sh "rm -rf *"
        try {
            checkout scm
        } catch (error) {
            emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                    replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                    to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                            [$class: 'RequesterRecipientProvider']]))
        }
        def rootDir = pwd()
        def selenium = load "${rootDir}/scripts/selenium_scripts.groovy"
        selenium.selenium_multi(current_test)
    }

    parallel shutdown_server: {
        node(env.SERVERNODE) {
            def rootDir = pwd()
            def logs = load "${rootDir}/scripts/logs.groovy"
            def node_shutdown = load "${rootDir}/scripts/node_shutdown.groovy"
            logs.get_archives_multi('$SERVERNODE','server')
            node_shutdown.linux_shutdown_script()
        }
    }, shutdown_node1: {
        node(env.NODENODE1) {
            def rootDir = pwd()
            def logs = load "${rootDir}/scripts/logs.groovy"
            def node_shutdown = load "${rootDir}/scripts/node_shutdown.groovy"
            logs.get_archives_multi('$NODENODE1', 'node')
            node_shutdown.windows_shutdown_script()
        }
    }, shutdown_node2: {
        node(env.NODENODE2) {
            def rootDir = pwd()
            def logs = load "${rootDir}/scripts/logs.groovy"
            def node_shutdown = load "${rootDir}/scripts/node_shutdown.groovy"
            logs.get_archives_multi('$NODENODE2', 'node')
            node_shutdown.mac_shutdown_script()
        }
    }, shutdown_node3: {
        node(env.NODENODE3) {
            def rootDir = pwd()
            def logs = load "${rootDir}/scripts/logs.groovy"
            def node_shutdown = load "${rootDir}/scripts/node_shutdown.groovy"
            logs.get_archives_multi('$NODENODE3', 'node')
            node_shutdown.linux_shutdown_script()
        }
    }


}
