def get_logs_mac(String testType) {
    sh 'echo Getting logs from $MACNODE'
    sh 'mkdir logs_$MACNODE-$STAGENAME'
    sh 'cp $HOME/.sethlans/logs/* logs_$MACNODE-$STAGENAME/'
    archiveArtifacts artifacts: '**/logs*/*', fingerprint: true
    if(testType.equals("render")){
        try {
            sh 'mkdir archives_$MACNODE-$STAGENAME'
            sh 'find $HOME/.sethlans/projects/. -name \'*.blend\' -delete'
            sh 'zip -j -r archives_$MACNODE-$STAGENAME/projects-$MACNODE-$STAGENAME.zip $HOME/.sethlans/projects/ -x "*part*.png"'
        } catch (error) {

        }
        try {
            archiveArtifacts artifacts: '**/archives*/', fingerprint: true
            sh 'rm -rf *'
        } catch (error) {

        }
    }

}

def get_logs_windows(String testType) {
    sh 'echo Getting logs from $WINNODE'
    sh 'mkdir logs_$WINNODE-$STAGENAME'
    sh 'cp $HOME/.sethlans/logs/* logs_$WINNODE-$STAGENAME/'
    archiveArtifacts artifacts: '**/logs*/*', fingerprint: true
    if(testType.equals("render")){
        try {
            sh 'mkdir archives_$WINNODE-$STAGENAME'
            sh 'find $HOME/.sethlans/projects/. -name \'*.blend\' -delete'
            sh 'zip -j -r archives_$WINNODE-$STAGENAME/projects-$WINNODE-$STAGENAME.zip $HOME/.sethlans/projects/ -x "*part*.png"'
        } catch (error) {

        }
        try {
            archiveArtifacts artifacts: '**/archives*/', fingerprint: true
            sh 'rm -rf *'
        } catch (error) {

        }
    }

}

def get_logs_linux(String testType) {
    sh 'echo Getting logs from $LINUXNODE'
    sh 'mkdir logs_$LINUXNODE-$STAGENAME'
    sh 'cp $HOME/.sethlans/logs/* logs_$LINUXNODE-$STAGENAME/'
    archiveArtifacts artifacts: '**/logs*/*', fingerprint: true
    if(testType.equals("render")){
        try {
            sh 'mkdir archives_$LINUXNODE-$STAGENAME'
            sh 'find $HOME/.sethlans/projects/. -name \'*.blend\' -delete'
            sh 'zip -j -r archives_$LINUXNODE-$STAGENAME/projects-$LINUXNODE-$STAGENAME.zip $HOME/.sethlans/projects/ -x "*part*.png"'
        } catch (error) {

        }
        try {
            archiveArtifacts artifacts: '**/archives*/', fingerprint: true
            sh 'rm -rf *'
        } catch (error) {

        }
    }

}

def get_archives_multi(String node, String type) {
    sh 'echo Getting logs from ' + node
    sh 'mkdir logs_' + node + '-$STAGENAME'
    sh 'cp $HOME/.sethlans/logs/* logs_' + node + '-$STAGENAME'
    archiveArtifacts artifacts: '**/logs*/*', fingerprint: true
    if(type.equals("server render")){
        try {
            sh 'mkdir archives_' + node + '-$STAGENAME'
            sh 'find $HOME/.sethlans/projects/. -name \'*.blend\' -delete'
            sh 'zip -j -r archives_' + node + '-$STAGENAME/projects-$STAGENAME.zip $HOME/.sethlans/projects/ -x "*part*.png"'
        } catch (error) {

        }
        try {
            archiveArtifacts artifacts: '**/archives*/', fingerprint: true
            sh 'rm -rf *'
        } catch (error) {

        }
    }

}

return this