
def windows_startup_script() {
    step([$class     : 'CopyArtifact',
          projectName: 'sethlans-build',
          filter     : 'target/binaries/*.exe'])
    script {
        try {
            timeout(time: 5, unit: 'SECONDS') {
                sh 'powershell kill -n javaw'
            }
        } catch (err) {
            echo "No running versions found"
        }
        try {
            timeout(time: 2, unit: 'SECONDS') {
                sh 'rm ~/sethlans_run/*'
            }
        } catch (err) {
            echo "No old builds present"
        }
        sh 'cp ~/jenkins_home/workspace/$JOB_NAME/target/binaries/Sethlans*.exe ~/sethlans_run/'
        sh 'rm -rf ~/.sethlans'
        sh 'rm -rf ~/tomcat*'
    }
    def HOSTNAME
    script {
        sh "hostname > temphostname"
        sh "tr -d '\\r' < temphostname > windowshostname"
        env.WINHOSTNAME = readFile 'windowshostname'
        HOSTNAME = readFile 'windowshostname'
        env.WINNODE = env.NODE_NAME
        withEnv(['JENKINS_NODE_COOKIE=dontKillMe']) {
            sh 'BUILD_ID=dontKillMe ~/sethlans_run/Sethlans*.exe &'
        }
        sleep(30) // In seconds

    }
    return HOSTNAME
}

def linux_startup_script() {
    step([$class     : 'CopyArtifact',
          projectName: 'sethlans-build',
          filter     : 'target/binaries/*.jar'])
    script {
        try {
            timeout(time: 5, unit: 'SECONDS') {
                sh 'pkill -f "java -jar /home/mestrella/sethlans_run/" '
            }
        } catch (err) {
            echo "No running versions found"
        }
        try {
            timeout(time: 2, unit: 'SECONDS') {
                sh 'rm ~/sethlans_run/*'
            }
        } catch (err) {
            echo "No old builds present"
        }
        sh 'cp ~/jenkins-build/workspace/$JOB_NAME/target/binaries/Sethlans*.jar ~/sethlans_run/'
        sh 'rm -rf ~/.sethlans'
        sh 'rm -rf ~/tomcat*'
    }
    def HOSTNAME
    script {
        env.LINUXHOSTNAME = readFile '/etc/hostname'
        HOSTNAME = readFile '/etc/hostname'
        env.LINUXNODE = env.NODE_NAME

        withEnv(['JENKINS_NODE_COOKIE=dontKillMe']) {
            sh 'nohup java -jar ~/sethlans_run/Sethlans*.jar &'
        }
        sleep(30) // In seconds
    }
    return HOSTNAME
}

def mac_startup_script() {
    step([$class     : 'CopyArtifact',
          projectName: 'sethlans-build',
          filter     : 'target/binaries/*.jar'])
    script {
        try {
            timeout(time: 5, unit: 'SECONDS') {
                sh 'pkill -f "/usr/bin/java -jar /Users/mestrella/sethlans_run/" '
            }
        } catch (err) {
            echo "No running versions found"
        }
        sh 'echo "Removing old Sethlans builds"'
        try {
            timeout(time: 2, unit: 'SECONDS') {
                sh 'rm $HOME/sethlans_run/*'
            }
        } catch (err) {
            echo "No old builds present"
        }
        sh 'cp ~/jenkins-agent/workspace/$JOB_NAME/target/binaries/Sethlans*.jar ~/sethlans_run/'
        sh 'rm -rf ~/.sethlans'
        sh 'rm -rf ~/tomcat*'
    }
    def HOSTNAME
    script {
        sh "hostname >> name"
        env.MACHOSTNAME = readFile 'name'
        HOSTNAME = readFile 'name'
        env.MACNODE = env.NODE_NAME

        withEnv(['JENKINS_NODE_COOKIE=dontKillMe']) {
            sh 'nohup java -jar ~/sethlans_run/Sethlans*.jar &'
        }
        sleep(30) // In seconds
    }
    return HOSTNAME
}
return this
