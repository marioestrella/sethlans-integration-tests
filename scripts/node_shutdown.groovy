def windows_shutdown_script() {
    script {
        sh 'echo "Starting Sethlans Windows Shutdown Script"'
        try {
            timeout(time: 5, unit: 'SECONDS') {
                sh 'powershell kill -n javaw'
                sh 'powershell kill -n nohup'
            }
        } catch (err) {
            echo "No running versions found"
        }
        try {
            sh 'rm -rf *'
            sh 'rm -rf ~/.sethlans'
            sh 'rm -rf ~/tomcat*'
        } catch (error) {

        }
        if(currentBuild.currentResult == 'UNSTABLE' || currentBuild.currentResult == 'FAILURE') {
            emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                    replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                    to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                            [$class: 'RequesterRecipientProvider']]))
        }
        sleep(30) // In seconds
    }
}

def linux_shutdown_script() {
    script {
        sh ' echo "Starting Sethlans Linux Shutdown Script"'
        try {
            timeout(time: 5, unit: 'SECONDS') {
                sh 'pkill -f "java -jar /home/mestrella/sethlans_run/" '
            }
        } catch (err) {
            echo "No running versions found"
        }
        try {
            sh 'rm -rf *'
            sh 'rm -rf ~/.sethlans'
            sh 'rm -rf ~/tomcat*'
        } catch (error) {

        }
        if(currentBuild.currentResult == 'UNSTABLE' || currentBuild.currentResult == 'FAILURE') {
            emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                    replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                    to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                            [$class: 'RequesterRecipientProvider']]))
        }
        sleep(30) // In seconds
    }

}

def mac_shutdown_script() {
    script {
        sh 'echo "Starting Sethlans Mac shutdown Script"'
        try {
            timeout(time: 5, unit: 'SECONDS') {
                sh 'pkill -f "/usr/bin/java -jar /Users/mestrella/sethlans_run/" '
            }
        } catch (err) {
            echo "No running versions found"
        }
        try {
            sh 'rm -rf *'
            sh 'rm -rf ~/.sethlans'
            sh 'rm -rf ~/tomcat*'
        } catch (error) {

        }

        if(currentBuild.currentResult == 'UNSTABLE' || currentBuild.currentResult == 'FAILURE') {
            emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                    replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                    to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                            [$class: 'RequesterRecipientProvider']]))
        }
        sleep(30) // In seconds
    }
}

return this
