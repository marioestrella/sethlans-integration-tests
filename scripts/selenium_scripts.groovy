def selenium_mac(String test) {
    sh "rm -rf *"
    try {
        checkout scm
    } catch (error) {
        emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                        [$class: 'RequesterRecipientProvider']]))
    }
    try {
        sh 'mkdir mvn_logs_$MACNODE-$STAGENAME'
        sh 'mvn ' + test + ' -Dos=$MACHOSTNAME' + ' -l mvn_logs_$MACNODE-$STAGENAME/mvn_log.log'
    } catch(err) {
        emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                        [$class: 'RequesterRecipientProvider']]))
    }

    junit '**/target/surefire-reports/*.xml'
    archiveArtifacts artifacts: '**/mvn_logs*/*', fingerprint: true
}

def selenium_linux(String test) {
    sh "rm -rf *"
    try {
        checkout scm
    } catch (error) {
        emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                        [$class: 'RequesterRecipientProvider']]))
    }
    try {
        sh 'mkdir mvn_logs_$LINUXNODE-$STAGENAME'
        sh 'mvn ' + test + ' -Dos=$LINUXHOSTNAME' + ' -l mvn_logs_$LINUXNODE-$STAGENAME/mvn_log.log'
    } catch(err){
        emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                        [$class: 'RequesterRecipientProvider']]))
    }
    junit '**/target/surefire-reports/*.xml'
    archiveArtifacts artifacts: '**/mvn_logs*/*', fingerprint: true

}

def selenium_windows(String test) {
    sh "rm -rf *"
    try {
        checkout scm
    } catch (error) {
        emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                        [$class: 'RequesterRecipientProvider']]))
    }
    try {
        sh 'mkdir mvn_logs_$WINNODE-$STAGENAME'
        sh 'mvn ' + test + ' -Dos=$WINHOSTNAME' + ' -l mvn_logs_$WINNODE-$STAGENAME/mvn_log.log'
    } catch(err) {
        emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                        [$class: 'RequesterRecipientProvider']]))
    }
    junit '**/target/surefire-reports/*.xml'
    archiveArtifacts artifacts: '**/mvn_logs*/*', fingerprint: true

}

def selenium_multi(String test) {
    String nodes = env.NODEHOST1 + ',' + env.NODEHOST2 + ',' + env.NODEHOST3
    nodes = nodes.replaceAll("\\n", "")
    nodes = nodes.replaceAll("\\s", "")

    println(nodes)
    sh "rm -rf *"
    try {
        checkout scm
    } catch (error) {
        emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                        [$class: 'RequesterRecipientProvider']]))
    }
    try {
        sh 'mkdir mvn_logs_$STAGENAME'
        sh 'mvn ' + test + ' -Dserver=$SERVERHOST' + '-Dnodes='+ nodes + ' -l mvn_logs_$STAGENAME/mvn_log.log'
    } catch(err) {
        emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                        [$class: 'RequesterRecipientProvider']]))
    }
    junit '**/target/surefire-reports/*.xml'
    archiveArtifacts artifacts: '**/mvn_logs*/*', fingerprint: true
}

def selenium_multi_2(String test) {
    String nodes = env.NODEHOST1 + ',' + env.NODEHOST2 + ',' +
            env.NODEHOST3 + ',' + env.NODEHOST4 + ',' +
            env.NODEHOST5 + ',' + env.NODEHOST6 + ',' +
            env.NODEHOST7 + ',' + env.NODEHOST8 + ',' +
            env.NODEHOST9
    nodes = nodes.replaceAll("\\n", "")
    nodes = nodes.replaceAll("\\s", "")

    println(nodes)
    sh "rm -rf *"
    try {
        checkout scm
    } catch (error) {
        emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                        [$class: 'RequesterRecipientProvider']]))
    }
    try {
        sh 'mkdir mvn_logs_$STAGENAME'
        sh 'mvn ' + test + ' -Dserver=$SERVERHOST' + '-Dnodes='+ nodes + ' -l mvn_logs_$STAGENAME/mvn_log.log'
    } catch(err) {
        emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                        [$class: 'RequesterRecipientProvider']]))
    }
    junit '**/target/surefire-reports/*.xml'
    archiveArtifacts artifacts: '**/mvn_logs*/*', fingerprint: true
}

return this
