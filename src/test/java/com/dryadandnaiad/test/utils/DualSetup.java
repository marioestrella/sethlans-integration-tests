package com.dryadandnaiad.test.utils;

import org.hamcrest.CoreMatchers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;

/**
 * Created Mario Estrella on 9/21/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class DualSetup {

    public void startSetup(WebDriverWait wait, WebDriver driver, String host, String hostname, String usernameString, String passwordString, Integer validPort, ComputeOptions computeOptions) throws InterruptedException {
        Utils.commentGenerator("Starting Sethlans Dual Setup");
        Thread.sleep(10000);
        WebElement nextButton = null;
        WebElement finishBtn = null;
        Utils.commentGenerator("Navigating to " + host);
        driver.get(host);
        Utils.commentGenerator("Configuring Settings");
        Thread.sleep(1000);
        WebElement username = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        WebElement password = driver.findElement(By.id("password"));
        try {
            Utils.slowCharEntry(username, usernameString);
            Utils.slowCharEntry(password, passwordString);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement passwordConfirm = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("passwordConfirm")));
        try {
            Utils.slowCharEntry(passwordConfirm, passwordString);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement port = driver.findElement(By.id("httpsPort"));
        port.clear();
        port.sendKeys(validPort.toString());
        Utils.setupClickNextButton(nextButton, wait);
        Thread.sleep(1000);
        Utils.commentGenerator("Selecting Mode - Dual");
        WebElement nodeButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mode3")));
        nodeButton.click();
        Utils.setupClickNextButton(nextButton, wait);
        String value = "";
        String gpuName = "";
        Utils.commentGenerator("Selecting Blender Version");
        Select blenderDropdown = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("blenderVersion"))));
        int count = blenderDropdown.getOptions().size();
        Random r = new Random();
        int blenderVersions = r.nextInt(count);
        blenderDropdown.selectByIndex(blenderVersions);
        WebElement selected = blenderDropdown.getFirstSelectedOption();
        String blenderVersion = selected.getAttribute("innerText");
        Select dualDropDown = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("selectedMethod"))));

        if (computeOptions.equals(ComputeOptions.CPU_GPU)) {
            if (dualDropDown.getOptions().size() > 1) {
                Utils.commentGenerator("Configuring Dual to use CPU & GPU");
                dualDropDown.selectByValue("CPU_GPU");
                value = Utils.selectCores(driver);
                gpuName = Utils.selectGPU(driver);
                WebElement gpuGroup = driver.findElement(By.id("cuda"));
                WebElement cpuGroup = driver.findElement(By.id("cpu"));
                CustomAssert.assertTrue("GPU Group Displayed", gpuGroup.isDisplayed());
                CustomAssert.assertTrue("CPU Group Displayed", cpuGroup.isDisplayed());
                Utils.setupClickNextButton(nextButton, wait);
                Thread.sleep(1000);
                Utils.commentGenerator("Sethlans Setup Summary");
                WebElement computeSummary = driver.findElement(By.xpath("//tr[contains(.,'Compute Method')]"));
                WebElement coreSummary = driver.findElement(By.xpath("//tr[contains(.,'Rendering Cores')]"));
                WebElement gpuSummary = driver.findElement(By.xpath("//tr[contains(.,'Selected GPU')]"));
                CustomAssert.assertThat("CPU Core Summary Match", coreSummary.getAttribute("innerText"),
                        CoreMatchers.containsString(value));
                CustomAssert.assertThat("GPU Graphics Card Match", gpuSummary.getAttribute("innerText"),
                        CoreMatchers.containsString(gpuName));
                CustomAssert.assertThat("CPU_GPU Compute Match", computeSummary.getAttribute("innerText"),
                        CoreMatchers.containsString("CPU & GPU"));
            } else {
                Utils.commentGenerator("Invalid Option: CPU and GPU is not available");
                host = "https://localhost:" + validPort + "/shutdown";
                Utils.cleanup(host, driver, wait, usernameString, passwordString);

            }
        }
        if (computeOptions.equals(ComputeOptions.GPU)) {
            if (dualDropDown.getOptions().size() > 1) {
                Utils.commentGenerator("Configuring Dual to use GPU");
                dualDropDown.selectByValue("GPU");
                WebElement gpuGroup = driver.findElement(By.id("cuda"));
                WebElement cpuGroup = driver.findElement(By.id("cpu"));
                CustomAssert.assertTrue("GPU Group Displayed", gpuGroup.isDisplayed());
                CustomAssert.assertTrue("CPU Group Not Displayed", !cpuGroup.isDisplayed());
                gpuName = Utils.selectGPU(driver);
                Utils.setupClickNextButton(nextButton, wait);
                Thread.sleep(1000);
                Utils.commentGenerator("Sethlans Setup Summary ");
                WebElement computeSummary = driver.findElement(By.xpath("//tr[contains(.,'Compute Method')]"));
                WebElement gpuSummary = driver.findElement(By.xpath("//tr[contains(.,'Selected GPU')]"));
                CustomAssert.assertThat("GPU Compute Match", computeSummary.getAttribute("innerText"),
                        CoreMatchers.containsString("GPU"));
                CustomAssert.assertThat("GPU Graphics Card Match", gpuSummary.getAttribute("innerText"),
                        CoreMatchers.containsString(gpuName));
            } else {
                Utils.commentGenerator("Invalid Option: GPU is not available");
                host = "https://localhost:" + validPort + "/shutdown";
                Utils.cleanup(host, driver, wait, usernameString, passwordString);
            }
        }
        if (computeOptions.equals(ComputeOptions.CPU)) {
            Utils.commentGenerator("Configuring Dual to use CPU");
            dualDropDown.selectByValue("CPU");
            value = Utils.selectCores(driver);
            WebElement gpuGroup = driver.findElement(By.id("cuda"));
            WebElement cpuGroup = driver.findElement(By.id("cpu"));
            CustomAssert.assertTrue("GPU Group Not Displayed", !gpuGroup.isDisplayed());
            CustomAssert.assertTrue("CPU Group Displayed", cpuGroup.isDisplayed());
            Utils.setupClickNextButton(nextButton, wait);
            Thread.sleep(1000);
            Utils.commentGenerator("Sethlans Setup Summary");
            WebElement computeSummary = driver.findElement(By.xpath("//tr[contains(.,'Compute Method')]"));
            WebElement coreSummary = driver.findElement(By.xpath("//tr[contains(.,'Rendering Cores')]"));
            CustomAssert.assertThat("CPU Core Summary Match", coreSummary.getAttribute("innerText"),
                    CoreMatchers.containsString(value));
            CustomAssert.assertThat("CPU Compute Summary Match", computeSummary.getAttribute("innerText"),
                    CoreMatchers.containsString("CPU"));
        }

        WebElement modeSummary = driver.findElement(By.xpath("//tr[contains(.,'Mode')]"));
        WebElement versionSummary = driver.findElement(By.xpath("//tr[contains(.,'Blender Version')]"));
        WebElement usernameSummary = driver.findElement(By.xpath("//tr[contains(.,'User Name')]"));
        WebElement portSummary = driver.findElement(By.xpath("//tr[contains(.,'HTTPS Port')]"));
        CustomAssert.assertThat("DUAL Summary Match", modeSummary.getAttribute("innerText"),
                CoreMatchers.containsString("Dual"));
        CustomAssert.assertThat("Username Match", usernameSummary.getAttribute("innerText"),
                CoreMatchers.containsString(usernameString.toLowerCase()));
        CustomAssert.assertThat("Port Sumamry Match", portSummary.getAttribute("innerText"),
                CoreMatchers.containsString(validPort.toString()));
        CustomAssert.assertThat("Blender Version Summary Match", versionSummary.getAttribute("innerText"),
                CoreMatchers.containsString(blenderVersion));
        Utils.setupClickFinishButton(finishBtn, wait);
        Utils.commentGenerator("Completed Setup - Sleeping for 60 seconds");
        Thread.sleep(60000);
        host = "https://" + hostname + ":" + validPort.toString() + "/";
        WebElement url = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("sethlansUrl")));
        CustomAssert.assertEquals("Expected URL Match", host, url.getAttribute("href"));
        Thread.sleep(5000);
        driver.quit();
    }
}
