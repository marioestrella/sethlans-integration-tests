package com.dryadandnaiad.test.utils;

import org.hamcrest.CoreMatchers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created Mario Estrella on 12/19/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class DualBenchmarkNode {

    public void startBenchmark(WebDriver driver, String sethlansOS, Integer validPort, String host,
                               ComputeOptions computeOptions, WebDriverWait wait) throws InterruptedException {

        int iend = sethlansOS.indexOf(".");
        if (iend != -1) {
            Utils.commentGenerator(sethlansOS + " contains a domain name. Removing it.");
            sethlansOS = sethlansOS.substring(0, iend);
        }

        String compute = null;

        switch (computeOptions) {
            case CPU:
                compute = "CPU";
                break;
            case GPU:
                compute = "GPU";
                break;
            case CPU_GPU:
                compute = "CPU & GPU";
        }


        WebElement nodesLink = driver.findElement(By.id("settings_nodes"));
        nodesLink.click();
        Thread.sleep(1000);
        Utils.commentGenerator("Adding Node Screen");
        WebElement addNodeLink = driver.findElement(By.id("addNodeBtn"));
        addNodeLink.click();
        Thread.sleep(1000);
        WebElement ipAddressorHostname = driver.findElement(By.id("ipAddress"));
        WebElement portField = driver.findElement(By.id("port"));
        WebElement nextButton = driver.findElement(By.id("nextButton"));
        Utils.slowCharEntry(ipAddressorHostname, sethlansOS);
        Utils.slowCharEntry(portField, validPort.toString());
        Thread.sleep(1000);
        nextButton.click();

        // Add Node Summary
        Utils.commentGenerator("Add Node Summary");
        Thread.sleep(3000);
        WebElement summaryTitle = driver.findElement(By.id("summary"));
        WebElement hostname = driver.findElement(By.id("hostname"));
        WebElement networkPort = driver.findElement(By.id("networkPort"));
        WebElement ipAddress = driver.findElement(By.id("ipAddress"));
        WebElement nodeOS = driver.findElement(By.id("nodeOS"));
        WebElement computeType = driver.findElement(By.id("computeType"));
        WebElement addNode = driver.findElement(By.id("addNode"));
        WebElement cores;
        WebElement selectedGPUs;
        WebElement cpuName;


        String hostnameValue = hostname.getAttribute("innerText");
        String nodeOSValue = nodeOS.getAttribute("innerText");
        String ipAddressValue = ipAddress.getAttribute("innerText");
        String computeTypeValue = computeType.getAttribute("innerText");
        String coresValue = null;
        String networkPortValue = networkPort.getAttribute("innerText");
        String selectedGPUsValue = null;
        String cpuNameValue = null;

        switch (computeOptions) {
            case CPU:
                cores = driver.findElement(By.id("cores"));
                cpuName = driver.findElement(By.id("cpuName"));
                coresValue = cores.getAttribute("innerText");
                cpuNameValue = cpuName.getAttribute("innerText");
                selectedGPUsValue = "N/A";
                break;
            case GPU:
                selectedGPUs = driver.findElement(By.id("selectedGPUs"));
                selectedGPUsValue = selectedGPUs.getAttribute("innerText");
                coresValue = "N/A";
                cpuNameValue = "N/A";
                break;
            case CPU_GPU:
                cores = driver.findElement(By.id("cores"));
                cpuName = driver.findElement(By.id("cpuName"));
                selectedGPUs = driver.findElement(By.id("selectedGPUs"));
                coresValue = cores.getAttribute("innerText");
                selectedGPUsValue = selectedGPUs.getAttribute("innerText");
                cpuNameValue = cpuName.getAttribute("innerText");
        }

        // Add Node Summary Assertions
        CustomAssert.assertEquals("Summary Screen Test", "Summary", summaryTitle.getAttribute("innerText"));
        CustomAssert.assertThat("Node Hostname Test", hostnameValue,
                CoreMatchers.containsString(sethlansOS.toUpperCase()));
        CustomAssert.assertNotNull("IP Address Present Test", ipAddressValue);
        CustomAssert.assertEquals("Network Port Test", validPort.toString(), networkPortValue);
        CustomAssert.assertNotNull("OS Present Test", nodeOSValue);
        CustomAssert.assertEquals("Compute Type Test", compute, computeTypeValue);

        switch (computeOptions) {
            case CPU:
                CustomAssert.assertNotNull("CPU Cores Present Test", coresValue);
                CustomAssert.assertNotNull("CPU Name Present Test", cpuNameValue);
                break;
            case GPU:
                CustomAssert.assertNotNull("Selected GPUs Test", selectedGPUsValue);
                break;
            case CPU_GPU:
                CustomAssert.assertNotNull("CPU Cores Present Test", coresValue);
                CustomAssert.assertNotNull("Selected GPUs Test", selectedGPUsValue);
        }

        Utils.commentGenerator("Adding Node: " + hostnameValue + " OS: " +
                nodeOSValue + " to database");
        Thread.sleep(1000);
        addNode.click();

        // Node List Verification
        String currentUrl = driver.getCurrentUrl();
        Thread.sleep(1000);
        WebElement nodeStatus = driver.findElement(By.cssSelector("td[id*='nodeStatus']"));
        WebElement nodeHostname = driver.findElement(By.cssSelector("td[id*='nodeHostname']"));
        WebElement nodeIpAddress = driver.findElement(By.cssSelector("td[id*='nodeIPAddress']"));
        WebElement nodeNetworkPort = driver.findElement(By.cssSelector("td[id*='nodeNetworkPort']"));
        nodeOS = driver.findElement(By.cssSelector("td[id*='nodeOS']"));
        WebElement nodeComputeType = driver.findElement(By.cssSelector("td[id*='nodeComputeType']"));
        WebElement nodeCores = driver.findElement(By.cssSelector("td[id*='nodeCores']"));
        WebElement nodeSelectedGPUs = driver.findElement(By.cssSelector("td[id*='nodeSelectedGPUs']"));
        WebElement nodeCPUName = driver.findElement(By.cssSelector("td[id*='nodeCPUName']"));
        WebElement nodeBenchmark = driver.findElement(By.cssSelector("td[id*='nodeBenchmarkRating']"));

        // Node List Assertions
        CustomAssert.assertEquals("Node List - Status Test", "Pending Activation", nodeStatus.getAttribute("innerText"));
        CustomAssert.assertEquals("Node List - URL Test", host + "settings/nodes/", currentUrl);
        CustomAssert.assertEquals("Node List - Hostname Test", hostnameValue, nodeHostname.getAttribute("innerText"));
        CustomAssert.assertEquals("Node List - IP Address Test", ipAddressValue, nodeIpAddress.getAttribute("innerText"));
        CustomAssert.assertEquals("Node List - Network Port Test", networkPortValue, nodeNetworkPort.getAttribute("innerText"));
        CustomAssert.assertEquals("Node List - Node OS Test", nodeOSValue, nodeOS.getAttribute("innerText"));
        CustomAssert.assertEquals("Node List - Node Compute Type", computeTypeValue, nodeComputeType.getAttribute("innerText"));
        CustomAssert.assertEquals("Node List - Node Cores", coresValue, nodeCores.getAttribute("innerText"));
        CustomAssert.assertEquals("Node List - Selected GPUs", selectedGPUsValue, nodeSelectedGPUs.getAttribute("innerText"));
        CustomAssert.assertEquals("Node List - Node CPU Name", cpuNameValue, nodeCPUName.getAttribute("innerText"));
        CustomAssert.assertEquals("Node List - Benchmark Rating", "N/A", nodeBenchmark.getAttribute("innerText"));
        Thread.sleep(1000);

        //Server Requests
        WebElement servers = driver.findElement(By.id("settings_servers"));
        servers.click();
        WebElement dropdown = driver.findElement(By.id("notifications"));
        dropdown.click();
        Thread.sleep(1000);
        Utils.commentGenerator("Checking for Presence of server request notification");
        dropdown = driver.findElement(By.id("notifications"));
        dropdown.click();
        Thread.sleep(1000);
        WebElement message = driver.findElement(By.id("message"));
        CustomAssert.assertThat("Server Request Notification Test",
                message.getAttribute("innerText"), CoreMatchers.containsString("New Server Request:"));

        // Server List Verification
        WebElement serverStatus = driver.findElement(By.cssSelector("td[id*='serverStatus']"));
        WebElement serverHostname = driver.findElement(By.cssSelector("td[id*='serverHostname']"));
        WebElement serverNetworkPort = driver.findElement(By.cssSelector("td[id*='serverNetworkPort']"));
        WebElement serverAcknowledgement = driver.findElement(By.cssSelector("a[id*='serverAcknowledgement']"));


        //Server List Assertions
        CustomAssert.assertEquals("Server List - Status Test",
                "Pending Acknowledgement", serverStatus.getAttribute("innerText"));
        CustomAssert.assertEquals("Server List - Hostname Test",
                hostnameValue, serverHostname.getAttribute("innerText"));
        CustomAssert.assertEquals("Server List - Network Port Test",
                networkPortValue, serverNetworkPort.getAttribute("innerText"));

        // Acknowledgement of Server
        serverAcknowledgement.click();
        Thread.sleep(1000);
        serverStatus = driver.findElement(By.cssSelector("td[id*='serverStatus']"));
        CustomAssert.assertEquals("Server List - Pending Server Response Status Test",
                "Pending Server Response", serverStatus.getAttribute("innerText"));
        servers = driver.findElement(By.id("settings_servers"));
        servers.click();
        Thread.sleep(1000);
        dropdown = driver.findElement(By.id("notifications"));
        dropdown.click();
        Thread.sleep(1000);
        message = driver.findElement(By.id("message"));
        CustomAssert.assertTrue("Server Request Notification Cleared Test",
                !message.getAttribute("innerText").contains("New Server Request"));
        serverStatus = driver.findElement(By.cssSelector("td[id*='serverStatus']"));
        CustomAssert.assertEquals("Server List - Active Status Test",
                "Active", serverStatus.getAttribute("innerText"));
        Thread.sleep(1000);

        nodesLink = driver.findElement(By.id("settings_nodes"));
        nodesLink.click();
        Thread.sleep(1000);
        nodeStatus = driver.findElement(By.cssSelector("td[id*='nodeStatus']"));
        CustomAssert.assertEquals("Node List - Status After Activation Test",
                "Pending Benchmark", nodeStatus.getAttribute("innerText"));
        Thread.sleep(1000);

        CheckBenchmarks.checkBenchmark(computeOptions, wait, driver);

    }




}
