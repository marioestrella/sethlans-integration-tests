package com.dryadandnaiad.test.utils;

import org.hamcrest.CoreMatchers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created Mario Estrella on 12/23/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class ServerAddNode {

    public void addNode(WebDriver driver, String usernameString, String passwordString, WebDriverWait wait, String nodeSethlans, Integer validPort, String host,
                        ComputeOptions computeOptions) throws InterruptedException {

        WebElement goHome = driver.findElement(By.id("home"));
        goHome.click();
        Thread.sleep(1000);
        WebElement menuButton = driver.findElement(By.id("settings"));
        menuButton.click();
        Utils.commentGenerator("Loading Settings");
        Thread.sleep(1000);

        int iend = nodeSethlans.indexOf(".");
        if (iend != -1) {
            Utils.commentGenerator(nodeSethlans + " contains a domain name. Removing it.");
            nodeSethlans = nodeSethlans.substring(0, iend);
        }

        String compute = null;

        switch (computeOptions) {
            case CPU:
                compute = "CPU";
                break;
            case GPU:
                compute = "GPU";
                break;
            case CPU_GPU:
                compute = "CPU & GPU";
        }


        WebElement nodesLink = driver.findElement(By.id("settings_nodes"));
        nodesLink.click();
        Thread.sleep(1000);
        Utils.commentGenerator("Adding Node Screen");
        WebElement addNodeLink = driver.findElement(By.id("addNodeBtn"));
        addNodeLink.click();
        Thread.sleep(1000);
        WebElement ipAddressorHostname = driver.findElement(By.id("ipAddress"));
        WebElement portField = driver.findElement(By.id("port"));
        WebElement nextButton = driver.findElement(By.id("nextButton"));
        Utils.slowCharEntry(ipAddressorHostname, nodeSethlans);
        Utils.slowCharEntry(portField, validPort.toString());
        Thread.sleep(1000);
        nextButton.click();

        // Add Node Summary
        Utils.commentGenerator("Add Node Summary");
        Thread.sleep(3000);
        WebElement summaryTitle = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("summary")));
        WebElement hostname = driver.findElement(By.id("hostname"));
        WebElement networkPort = driver.findElement(By.id("networkPort"));
        WebElement ipAddress = driver.findElement(By.id("ipAddress"));
        WebElement nodeOS = driver.findElement(By.id("nodeOS"));
        WebElement computeType = driver.findElement(By.id("computeType"));
        WebElement addNode = driver.findElement(By.id("addNode"));
        WebElement cores;
        WebElement selectedGPUs;
        WebElement cpuName;


        String hostnameValue = hostname.getAttribute("innerText");
        String nodeOSValue = nodeOS.getAttribute("innerText");
        String ipAddressValue = ipAddress.getAttribute("innerText");
        String computeTypeValue = computeType.getAttribute("innerText");
        String coresValue = null;
        String networkPortValue = networkPort.getAttribute("innerText");
        String selectedGPUsValue = null;
        String cpuNameValue = null;

        switch (computeOptions) {
            case CPU:
                cores = driver.findElement(By.id("cores"));
                cpuName = driver.findElement(By.id("cpuName"));
                coresValue = cores.getAttribute("innerText");
                cpuNameValue = cpuName.getAttribute("innerText");
                selectedGPUsValue = "N/A";
                break;
            case GPU:
                selectedGPUs = driver.findElement(By.id("selectedGPUs"));
                selectedGPUsValue = selectedGPUs.getAttribute("innerText");
                coresValue = "N/A";
                cpuNameValue = "N/A";
                break;
            case CPU_GPU:
                cores = driver.findElement(By.id("cores"));
                cpuName = driver.findElement(By.id("cpuName"));
                selectedGPUs = driver.findElement(By.id("selectedGPUs"));
                coresValue = cores.getAttribute("innerText");
                selectedGPUsValue = selectedGPUs.getAttribute("innerText");
                cpuNameValue = cpuName.getAttribute("innerText");
        }

        // Add Node Summary Assertions
        CustomAssert.assertEquals("Summary Screen Test", "Summary", summaryTitle.getAttribute("innerText"));
        CustomAssert.assertThat("Node Hostname Test", hostnameValue,
                CoreMatchers.containsString(nodeSethlans.toUpperCase()));
        CustomAssert.assertNotNull("IP Address Present Test", ipAddressValue);
        CustomAssert.assertEquals("Network Port Test", validPort.toString(), networkPortValue);
        CustomAssert.assertNotNull("OS Present Test", nodeOSValue);
        CustomAssert.assertEquals("Compute Type Test", compute, computeTypeValue);

        switch (computeOptions) {
            case CPU:
                CustomAssert.assertNotNull("CPU Cores Present Test", coresValue);
                CustomAssert.assertNotNull("CPU Name Present Test", cpuNameValue);
                break;
            case GPU:
                CustomAssert.assertNotNull("Selected GPUs Test", selectedGPUsValue);
                break;
            case CPU_GPU:
                CustomAssert.assertNotNull("CPU Cores Present Test", coresValue);
                CustomAssert.assertNotNull("Selected GPUs Test", selectedGPUsValue);
        }

        Utils.commentGenerator("Adding Node: " + hostnameValue + " OS: " +
                nodeOSValue + " to database");
        Thread.sleep(1000);
        addNode.click();


    }
}
