package com.dryadandnaiad.test.utils;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created Mario Estrella on 9/5/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class Utils {

    public static String usernameGenerator(int value){
        commentGenerator("Generating Username");
        String generatedUser;
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        generatedUser = RandomStringUtils.random(value, characters);
        return generatedUser;
    }

    public static String passwordGenerator(int value){
        commentGenerator("Generating Password");
        String generatedPassword;
//        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?";
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*?";
        generatedPassword = RandomStringUtils.random(value, characters);
        return generatedPassword;

    }

    public static String getHostURL(String hostname, Integer port) {
        return "https://" + hostname + ":" + port + "/";
    }

    public static String invalidPasswordGenerator(int value) {
        commentGenerator("Generating Invalid Password");
        String generatedroot = passwordGenerator(value);
        String characters = "()-_=+[{]}\\|;:\'\",<.>/";
        return RandomStringUtils.random(value, characters + generatedroot);
    }

    public static String invalidUsernameGenerator(int value) {
        commentGenerator("Generating Invalid Username");
        String generatedroot = usernameGenerator(value);
        String characters = "~`!@#$%^&*()-_=+[{]}\\\\|;:\\'\\\",<.>/?";
        return RandomStringUtils.random(value, characters + generatedroot);
    }

    public static Integer validPortGenerator(){
        commentGenerator("Generating random port");
        Integer validPort;
        Random r = new Random();
        int low = 1;
        int high = 65535;
        validPort = r.nextInt(high-low) + low;
        return validPort;
    }

    public static Integer validPortGenerator(int low, int high) {
        commentGenerator("Generating a port from the following range: " + low + "-" + high);
        Integer validPort;
        Random r = new Random();
        validPort = r.nextInt(high - low) + low;
        return validPort;
    }

    public static String projectNameGenerator() {
        commentGenerator("Selecting Project Name");
        List<String> projectArray = new ArrayList<String>() {{
            add("Lorem ipsum dolor sit amet");
            add("Excepteur sint occaecat cupidatat");
            add("Ut enim ad minim veniam");
            add("Feugiat scelerisque varius");
            add("Sed turpis tincidunt");
            add("Purus semper eget");
            add("Consectetur purus ut faucibus");
            add("Est ante in nibh");
            add("Ornare suspendisse sed nisi");
            add("Magna fermentum iaculis eu");
            add("Vitae justo eget magna");
            add("Duis aute irure dolor in reprehenderit");
            add("Lectus quam id leo in");
            add("Eu facilisis sed odio morbi quis");
            add("Scelerisque purus semper eget duis");
            add("Amet luctus venenatis lectus magna");
            add("Dio pellentesque diam volutpat commodo sed egestas");
            add("Nisl pretium fusce id velit ut tortor pretium");
            add("Porttitor rhoncus dolor purus non enim");
            add("Suspendisse faucibus interdum posuere");
            add("Morbi tristique senectus et netus");
            add("Condimentum mattis pellentesque id nibh tortor id aliquet");
            add("Cursus turpis massa tincidunt dui ut ornare lectus sit amet");
            add("Arcu risus quis varius quam");
            add("Ac feugiat sed lectus vestibulum");
        }};
        int rnd = new Random().nextInt(projectArray.size());
        String projectName = projectArray.get(rnd);
        commentGenerator(projectName + " will be used");
        return projectName;
    }

    public static void moveSlider(WebDriver driver, WebElement slider) {
        commentGenerator("Moving Slider");
        Random r = new Random();
        int value = r.nextInt(20) + 1;
        value = value * 10;
        Actions moveSlider = new Actions(driver);
        moveSlider.moveToElement(slider).click().dragAndDropBy(slider, value, 0).build().perform();
    }

    public static void setupClickNextButton(WebElement nextButton, WebDriverWait wait) {
        commentGenerator("Clicking Next Button on Setup Wizard");
        nextButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("nextButton")));
        nextButton.click();
    }

    public static void setupClickFinishButton(WebElement finishButton, WebDriverWait wait) {
        commentGenerator("Clicking Finish Button on Setup Wizard");
        finishButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("finishBtn")));
        finishButton.click();
    }

    public static void setDriver() {
        if (SystemUtils.IS_OS_MAC) {
            commentGenerator("Setting MacOS Gecko Driver");
            System.setProperty("webdriver.gecko.driver", "/opt/geckodriver/geckodriver");
            System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");

        }
        if (SystemUtils.IS_OS_WINDOWS) {
            commentGenerator("Setting Windows Gecko Driver");
            System.setProperty("webdriver.gecko.driver", "C:\\opt\\geckodriver\\geckodriver.exe");
        }
        System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "/dev/null");

    }

    public static void cleanup(String host, WebDriver driver, WebDriverWait wait, String usernameString, String passwordString) {
        // The following just sends the shutdown URL to the host and ignores the exception
        try {
            driver.get(host);
            Login login = new Login();
            login.sethlansLogin(usernameString, passwordString, wait, driver);
        } catch (Exception ex) {
            commentGenerator("Sethlans Shutdown Called");
        }
        driver.quit();
    }

    public static String selectCores(WebDriver driver) {
        commentGenerator("Selecting CPU Cores");
        WebElement coreSlider = driver.findElement(By.className("slider-handle"));
        Utils.moveSlider(driver, coreSlider);
        WebElement cores = driver.findElement(By.id("cores"));
        return cores.getAttribute("value");
    }


    public static void selectMaxSliderValue(WebDriver driver, WebElement input) {
        commentGenerator("Selecting Max Slider Value");
        WebElement slider = input.findElement(By.className("slider-handle"));
        commentGenerator("Moving Slider");
        int value = 20 * 10;
        Actions moveSlider = new Actions(driver);
        moveSlider.moveToElement(slider).click().dragAndDropBy(slider, value, 0).build().perform();
    }

    private static void loadHome(WebDriver driver, String usernameString, String passwordString, WebDriverWait wait, String host) throws InterruptedException {
        commentGenerator("Loading HomePage");
        driver.get(host + "home");
        Login login = new Login();
        login.sethlansLogin(usernameString, passwordString, wait, driver);
        Thread.sleep(8000);
    }

    public static String selectGPU(WebDriver driver) {
        commentGenerator("Selecting GPU Cores");
        WebElement gpuCheckbox = driver.findElement(By.id("0"));
        WebElement gpuLabel = driver.findElement(By.id("0-label"));
        String gpuName = gpuLabel.getAttribute("innerText");
        CustomAssert.assertTrue("CPU Checkbox Not Selected", !gpuCheckbox.isSelected());
        gpuCheckbox.click();
        return gpuName;
    }

    public static void slowCharEntry(WebElement webElement, String string) throws InterruptedException {
        for (Character letter : string.toCharArray()) {
            webElement.sendKeys(letter.toString());
            Thread.sleep(50);
        }
    }

    public static void commentGenerator(String comment) {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        Date today = Calendar.getInstance().getTime();
        String date = df.format(today);
        System.out.println(date + ": " + comment + System.lineSeparator());
    }

    public static WebElement modeConfirmation(WebDriver driver, WebDriverWait wait, String host, String usernameString, String passwordString) throws InterruptedException {
        WebElement modeConfirm;
        try {
            Utils.commentGenerator("First wait for sethlansMode");
            modeConfirm = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("sethlansMode")));
            return modeConfirm;
        } catch (NoSuchElementException | TimeoutException ex) {
            Utils.loadHome(driver, usernameString, passwordString, wait, host);
            Utils.commentGenerator("Attempting to wait for sethlansMode again");
            modeConfirm = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("sethlansMode")));
            return modeConfirm;
        }
    }
}
