package com.dryadandnaiad.test.utils;

import org.hamcrest.CoreMatchers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;


/**
 * Created Mario Estrella on 9/21/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class ServerSetup {

    public void startSetup(WebDriverWait wait, WebDriver driver, String host, String hostname, String usernameString, String passwordString, Integer validPort) throws InterruptedException {
        Utils.commentGenerator("Starting Sethlans Server Setup");
        Thread.sleep(10000);
        WebElement nextButton = null;
        WebElement finishBtn = null;
        Random r = new Random();
        Utils.commentGenerator("Navigating to " + host);
        driver.get(host);
        Utils.commentGenerator("Configuring Settings");
        Thread.sleep(1000);
        WebElement username = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        WebElement password = driver.findElement(By.id("password"));
        try {
            Utils.slowCharEntry(username, usernameString);
            Utils.slowCharEntry(password, passwordString);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement passwordConfirm = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("passwordConfirm")));
        try {
            Utils.slowCharEntry(passwordConfirm, passwordString);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement port = driver.findElement(By.id("httpsPort"));
        port.clear();
        port.sendKeys(validPort.toString());
        Utils.setupClickNextButton(nextButton, wait);
        Thread.sleep(1000);
        Utils.commentGenerator("Selecting Mode - Server");
        WebElement serverButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mode1")));
        serverButton.click();
        Utils.setupClickNextButton(nextButton, wait);
        Thread.sleep(1000);
        Utils.commentGenerator("Selecting Blender Version");
        Select blenderDropdown = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("blenderVersion"))));
        int count = blenderDropdown.getOptions().size();
        int value = r.nextInt(count);
        blenderDropdown.selectByIndex(value);
        WebElement selected = blenderDropdown.getFirstSelectedOption();
        String blenderVersion = selected.getAttribute("innerText");
        Utils.setupClickNextButton(nextButton, wait);
        Thread.sleep(1000);
        Utils.commentGenerator("Sethlans Setup Summary");
        WebElement versionSummary = driver.findElement(By.xpath("//tr[contains(.,'Blender Version')]"));
        WebElement modeSummary = driver.findElement(By.xpath("//tr[contains(.,'Mode')]"));
        WebElement usernameSummary = driver.findElement(By.xpath("//tr[contains(.,'User Name')]"));
        WebElement portSummary = driver.findElement(By.xpath("//tr[contains(.,'HTTPS Port')]"));
        CustomAssert.assertThat("SERVER Summary Match", modeSummary.getAttribute("innerText"),
                CoreMatchers.containsString("Server"));
        CustomAssert.assertThat("Blender Version Summary Match", versionSummary.getAttribute("innerText"),
                CoreMatchers.containsString(blenderVersion));
        CustomAssert.assertThat("Username Match", usernameSummary.getAttribute("innerText"),
                CoreMatchers.containsString(usernameString.toLowerCase()));
        CustomAssert.assertThat("Port Summary Match", portSummary.getAttribute("innerText"),
                CoreMatchers.containsString(validPort.toString()));
        Utils.setupClickFinishButton(finishBtn, wait);
        Utils.commentGenerator("Completed Setup - Sleeping for 30 seconds");
        Thread.sleep(30000);
        host = "https://" + hostname + ":" + validPort.toString() + "/";
        WebElement url = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("sethlansUrl")));
        CustomAssert.assertEquals("Expected URL Match", host, url.getAttribute("href"));
        Thread.sleep(5000);
        driver.quit();
    }
}
