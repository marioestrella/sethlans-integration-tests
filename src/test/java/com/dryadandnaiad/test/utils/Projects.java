package com.dryadandnaiad.test.utils;

import org.hamcrest.CoreMatchers;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created Mario Estrella on 10/25/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class Projects {

    public Integer createStillImageProject(WebDriver driver, String host, WebDriverWait wait,
                                           String filename, ComputeOptions computeOptions, EngineOptions engineOptions,
                                           boolean defaultRes, String resX, String resY, boolean defaultSamples, String numOfSamples,
                                           boolean setMaxParts, boolean deleteProject)
            throws InterruptedException {
        WebElement goHome = driver.findElement(By.id("home"));
        goHome.click();

        if (engineOptions == EngineOptions.BLENDER_RENDER) {
            computeOptions = ComputeOptions.CPU;
        }

        String engine = null;
        String compute = null;

        switch (engineOptions) {
            case CYCLES:
                engine = "Cycles";
                break;
            case BLENDER_RENDER:
                engine = "Blender Render";
        }

        switch (computeOptions) {
            case CPU:
                compute = "CPU";
                break;
            case GPU:
                compute = "GPU";
                break;
            case CPU_GPU:
                compute = "CPU & GPU";
        }

        Thread.sleep(1000);
        WebElement menuItemProject = driver.findElement(By.id("projects"));
        menuItemProject.click();
        Utils.commentGenerator("Loading Project List");
        Thread.sleep(2000);
        String currentUrl = driver.getCurrentUrl();
        CustomAssert.assertEquals("Project List URL", host + "project", currentUrl);
        WebElement newProjectBtn = driver.findElement(By.id("project_new"));
        Thread.sleep(2000);
        newProjectBtn.click();
        Utils.commentGenerator("Starting New Project Wizard");
        Thread.sleep(1000);
        currentUrl = driver.getCurrentUrl();
        CustomAssert.assertEquals("New Project URL", host + "project/new", currentUrl);


        // Project File Upload Form
        Utils.commentGenerator("Project File Upload Form");
        WebElement projectForm = driver.findElement(By.id("projectWizard"));
        WebElement projectUpload = driver.findElement(By.id("projectUpload"));
        String currentPath = Paths.get(".").toAbsolutePath().normalize().toString();
        String projectFile = currentPath + "/sethlans-files/blend_files/" + filename;
        Utils.commentGenerator("Current file to test " + projectFile);
        projectUpload.sendKeys(projectFile);
        projectForm.submit();
        Thread.sleep(5000);

        // Project Details Form
        Utils.commentGenerator("Project Details Form");
        WebElement projectFormTitle = driver.findElement(By.id("projectFormTitle"));
        CustomAssert.assertEquals("Project Details Heading", "Project Details", projectFormTitle.getAttribute("innerText"));

        // Enter Project Name
        WebElement projectName = driver.findElement(By.id("projectName"));
        String sampleProjectName = Utils.projectNameGenerator();
        Utils.slowCharEntry(projectName, sampleProjectName);
        Thread.sleep(1000);

        // Select Blender Version
        Select blenderVersion = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("selectedBlenderVersion"))));
        int count = blenderVersion.getOptions().size();
        Random r = new Random();
        int value = r.nextInt(count);
        blenderVersion.selectByIndex(value);
        WebElement selected = blenderVersion.getFirstSelectedOption();
        String selectedBlenderVersion = selected.getAttribute("innerText");
        Thread.sleep(1000);


        // Select Project Type
        WebElement stillImage = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("projectType1")));
        stillImage.click();
        Thread.sleep(1000);


        // Select Compute Type - RenderOn
        if (engineOptions == EngineOptions.CYCLES) {
            Select computeDropdown = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("renderOn"))));
            computeDropdown.selectByValue(computeOptions.toString());
        }

        // Select Rendering Engine
        Select renderEngine = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("blenderEngine"))));
        renderEngine.selectByValue(engineOptions.toString());
        // Samples
        String numberOfSamples = null;
        if (engineOptions == EngineOptions.CYCLES) {
            WebElement samples = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("samples")));
            if (!defaultSamples) {
                samples.clear();
                samples.sendKeys(numOfSamples);
            }
            numberOfSamples = samples.getAttribute("value");
        }
        Thread.sleep(1000);


        // Enter Resolution
        WebElement resolutionX = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("resolutionX")));
        WebElement resolutionY = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("resolutionY")));
        if (!defaultRes) {
            resolutionX.clear();
            resolutionY.clear();
            resolutionX.sendKeys(resX);
            resolutionY.sendKeys(resY);
        }
        Thread.sleep(1000);
        String xValue = resolutionX.getAttribute("value");
        String yValue = resolutionY.getAttribute("value");

        // Image Scaling
        WebElement scaling = driver.findElement(By.id("scaling"));
        String scalingValue = scaling.getAttribute("value");

        if (setMaxParts) {
            JavascriptExecutor js = ((JavascriptExecutor) driver);
            js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
            WebElement parts = driver.findElement(By.id("partsGroup"));
            Utils.selectMaxSliderValue(driver, parts);
        }

        // Submit Project
        WebElement projectSubmit = driver.findElement(By.id("projectSubmit"));
        projectSubmit.click();
        Utils.commentGenerator("Project Submitted");
        Thread.sleep(2000);
        currentUrl = driver.getCurrentUrl();
        CustomAssert.assertThat("Project Submission Verification URL", currentUrl,
                CoreMatchers.containsString(host + "project/view/"));

        // Review Project Details
        Utils.commentGenerator("Verifying Project Details");
        projectName = driver.findElement(By.id("projectName"));
        WebElement projectIdNum = driver.findElement(By.id("projectNum"));
        WebElement projectBlenderVersion = driver.findElement(By.id("projectBlenderVersion"));
        WebElement projectType = driver.findElement(By.id("projectType"));
        WebElement projectBlenderEngine = driver.findElement(By.id("projectBlenderEngine"));
        WebElement projectComputeType = driver.findElement(By.id("computeType"));
        WebElement projectResolution = driver.findElement(By.id("resolution"));
        WebElement projectResPercentage = driver.findElement(By.id("resPercentage"));
        WebElement projectFrame = driver.findElement(By.id("frameNumber"));
        WebElement projectSamples = null;


        if (engineOptions == EngineOptions.CYCLES) {
            projectSamples = driver.findElement(By.id("samples"));
        }

        Integer projectId = Integer.valueOf(projectIdNum.getAttribute("innerText"));


        // Project Detail Assertions
        Utils.commentGenerator("Project Detail Assertions");
        CustomAssert.assertEquals("Selected Blender Test", selectedBlenderVersion, projectBlenderVersion.getAttribute("innerText"));
        CustomAssert.assertEquals("Project Frame Test", "1", projectFrame.getAttribute("innerText"));
        CustomAssert.assertEquals("Scaling Value Test", scalingValue + "%", projectResPercentage.getAttribute("innerText"));
        CustomAssert.assertEquals("Resolution Test", xValue + " x " + yValue, projectResolution.getAttribute("innerText"));
        CustomAssert.assertEquals("Compute Type Test", compute, projectComputeType.getAttribute("innerText"));
        CustomAssert.assertEquals("Render Engine Test", engine, projectBlenderEngine.getAttribute("innerText"));
        CustomAssert.assertEquals("Project Type Test", "Still Image", projectType.getAttribute("innerText"));
        CustomAssert.assertEquals("Project Name Test", sampleProjectName, projectName.getAttribute("innerText"));

        if (engineOptions == EngineOptions.CYCLES) {
            CustomAssert.assertEquals("Number of Samples Test", numberOfSamples, projectSamples.getAttribute("innerText"));
        }

        // Verify Project List
        Utils.commentGenerator("Returning to Project List");
        WebElement projectReturn = driver.findElement(By.id("project_list"));
        projectReturn.click();
        Thread.sleep(1000);
        if (deleteProject) {
            currentUrl = driver.getCurrentUrl();
            CustomAssert.assertEquals("Project List URL Test", host + "project", currentUrl);
            projectName = driver.findElement(By.xpath("//*[contains(@id, 'projectName')]"));
            projectType = driver.findElement(By.xpath("//*[contains(@id, 'projectType')]"));
            WebElement projectDelete = driver.findElement(By.xpath("//*[contains(@id, 'projectDelete')]"));
            projectComputeType = driver.findElement(By.xpath("//*[contains(@id, 'projectComputeType')]"));
            projectResolution = driver.findElement(By.xpath("//*[contains(@id, 'projectResolution')]"));

            // Project List Assertions
            Utils.commentGenerator("Project List Assertions");
            CustomAssert.assertEquals("Project List - Resolution Test", xValue + " x " + yValue,
                    projectResolution.getAttribute("innerText"));
            CustomAssert.assertEquals("Project List - Compute Type Test", compute,
                    projectComputeType.getAttribute("innerText"));
            CustomAssert.assertEquals("Project List - Project Type Test", "Still Image",
                    projectType.getAttribute("innerText"));
            CustomAssert.assertEquals("Project List - Project Name Test", sampleProjectName, projectName.getAttribute("innerText"));
            Thread.sleep(1000);

            deleteProject(projectDelete);
        }
        return projectId;
    }

    public Integer createAnimationProject(WebDriver driver, String host, WebDriverWait wait,
                                          String filename, ComputeOptions computeOptions, EngineOptions engineOptions,
                                          boolean defaultRes, String resX, String resY, boolean defaultSamples, String numOfSamples, boolean useRandomFrames,
                                          Integer startFrameValue, Integer endFrameValue, Integer frameStepValue, boolean useRandomFormat,
                                          RenderOutputFormat renderOutputFormat, boolean setMaxParts,
                                          boolean deleteProject)
            throws InterruptedException {
        WebElement goHome = driver.findElement(By.id("home"));
        goHome.click();

        if (engineOptions == EngineOptions.BLENDER_RENDER) {
            computeOptions = ComputeOptions.CPU;
        }

        String engine = null;
        String compute = null;

        switch (engineOptions) {
            case CYCLES:
                engine = "Cycles";
                break;
            case BLENDER_RENDER:
                engine = "Blender Render";
        }

        switch (computeOptions) {
            case CPU:
                compute = "CPU";
                break;
            case GPU:
                compute = "GPU";
                break;
            case CPU_GPU:
                compute = "CPU & GPU";
        }


        Thread.sleep(1000);
        WebElement menuItemProject = driver.findElement(By.id("projects"));
        menuItemProject.click();
        Utils.commentGenerator("Loading Project List");
        Thread.sleep(2000);
        String currentUrl = driver.getCurrentUrl();
        CustomAssert.assertEquals("Project List URL", host + "project", currentUrl);
        WebElement newProjectBtn = driver.findElement(By.id("project_new"));
        Thread.sleep(2000);
        newProjectBtn.click();
        Utils.commentGenerator("Starting New Project Wizard");
        Thread.sleep(1000);
        currentUrl = driver.getCurrentUrl();
        CustomAssert.assertEquals("New Project URL", host + "project/new", currentUrl);

        // Project File Upload Form
        Utils.commentGenerator("Project File Upload Form");
        WebElement projectForm = driver.findElement(By.id("projectWizard"));
        WebElement projectUpload = driver.findElement(By.id("projectUpload"));
        String currentPath = Paths.get(".").toAbsolutePath().normalize().toString();
        String projectFile = currentPath + "/sethlans-files/blend_files/" + filename;
        Utils.commentGenerator("Current file to test " + projectFile);
        projectUpload.sendKeys(projectFile);
        projectForm.submit();
        Thread.sleep(5000);

        // Project Details Form
        Utils.commentGenerator("Project Details Form");
        WebElement projectFormTitle = driver.findElement(By.id("projectFormTitle"));
        CustomAssert.assertEquals("Project Details Heading", "Project Details", projectFormTitle.getAttribute("innerText"));
        // Enter Project Name
        WebElement projectName = driver.findElement(By.id("projectName"));
        String sampleProjectName = Utils.projectNameGenerator();
        Utils.slowCharEntry(projectName, sampleProjectName);
        // Select Blender Version
        Select blenderVersion = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("selectedBlenderVersion"))));
        int count = blenderVersion.getOptions().size();
        Random r = new Random();
        int value = r.nextInt(count);
        blenderVersion.selectByIndex(value);
        WebElement selected = blenderVersion.getFirstSelectedOption();
        String selectedBlenderVersion = selected.getAttribute("innerText");

        // Select Project Type
        WebElement animation = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("projectType2")));
        animation.click();

        // Select Render Output Format
        Select outputFormat = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("outputFormat"))));
        if (useRandomFormat) {
            count = outputFormat.getOptions().size();
            r = new Random();
            value = r.nextInt(count);
            outputFormat.selectByIndex(value);
            selected = outputFormat.getFirstSelectedOption();
        } else {
            outputFormat.selectByValue(renderOutputFormat.name());
            selected = outputFormat.getFirstSelectedOption();
        }

        String selectedOutputFormat = selected.getAttribute("innerText");

        //Set Frames
        WebElement startFrame = driver.findElement(By.id("startframe"));
        WebElement endFrame = driver.findElement(By.id("endframe"));
        WebElement frameStep = driver.findElement(By.id("framestep"));
        r = new Random();
        if (useRandomFrames) {
            startFrameValue = r.nextInt(99) + 1;
            endFrameValue = r.nextInt(2000 - startFrameValue) + startFrameValue;
            frameStepValue = r.nextInt(20) + 1;
        }


        startFrame.clear();
        endFrame.clear();
        frameStep.clear();
        Utils.slowCharEntry(startFrame, startFrameValue.toString());
        Utils.slowCharEntry(endFrame, endFrameValue.toString());
        Utils.slowCharEntry(frameStep, frameStepValue.toString());


        // Select Compute Type - RenderOn
        if (engineOptions == EngineOptions.CYCLES) {
            Select computeDropdown = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("renderOn"))));
            computeDropdown.selectByValue(computeOptions.toString());
        }

        // Select Rendering Engine
        Select renderEngine = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("blenderEngine"))));
        renderEngine.selectByValue(engineOptions.toString());
        // Samples
        String numberOfSamples = null;
        if (engineOptions == EngineOptions.CYCLES) {
            WebElement samples = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("samples")));
            if (!defaultSamples) {
                samples.clear();
                samples.sendKeys(numOfSamples);
            }
            numberOfSamples = samples.getAttribute("value");
        }

        // Enter Resolution
        WebElement resolutionX = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("resolutionX")));
        WebElement resolutionY = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("resolutionY")));
        if (!defaultRes) {
            resolutionX.clear();
            resolutionY.clear();
            resolutionX.sendKeys(resX);
            resolutionY.sendKeys(resY);
        }
        String xValue = resolutionX.getAttribute("value");
        String yValue = resolutionY.getAttribute("value");

        // Image Scaling
        WebElement scaling = driver.findElement(By.id("scaling"));
        String scalingValue = scaling.getAttribute("value");

        if (setMaxParts) {
            JavascriptExecutor js = ((JavascriptExecutor) driver);
            js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
            WebElement parts = driver.findElement(By.id("partsGroup"));
            Utils.selectMaxSliderValue(driver, parts);
        }

        // Submit Project
        WebElement projectSubmit = driver.findElement(By.id("projectSubmit"));
        projectSubmit.click();
        Utils.commentGenerator("Project Submitted");
        Thread.sleep(2000);
        currentUrl = driver.getCurrentUrl();
        CustomAssert.assertThat("Project Submission Verification URL", currentUrl,
                CoreMatchers.containsString(host + "project/view/"));


        // Review Project Details
        Utils.commentGenerator("Verifying Project Details");
        projectName = driver.findElement(By.id("projectName"));
        WebElement projectIdNum = driver.findElement(By.id("projectNum"));
        WebElement projectBlenderVersion = driver.findElement(By.id("projectBlenderVersion"));
        WebElement projectType = driver.findElement(By.id("projectType"));
        WebElement projectBlenderEngine = driver.findElement(By.id("projectBlenderEngine"));
        WebElement projectComputeType = driver.findElement(By.id("computeType"));
        WebElement projectResolution = driver.findElement(By.id("resolution"));
        WebElement projectResPercentage = driver.findElement(By.id("resPercentage"));
        WebElement projectStartFrame = driver.findElement(By.id("startFrame"));
        WebElement projectEndFrame = driver.findElement(By.id("endFrame"));
        WebElement projectFrameStep = driver.findElement(By.id("frameStep"));
        WebElement projectRenderOutputFormat = driver.findElement(By.id("renderOutputFormat"));
        WebElement projectSamples = null;

        Integer projectId = Integer.valueOf(projectIdNum.getAttribute("innerText"));


        if (engineOptions == EngineOptions.CYCLES) {
            projectSamples = driver.findElement(By.id("samples"));
        }

        // Project Detail Assertions
        Utils.commentGenerator("Project Detail Assertions");
        CustomAssert.assertEquals("Selected Blender Test", selectedBlenderVersion, projectBlenderVersion.getAttribute("innerText"));
        CustomAssert.assertEquals("Project Start Frame Test", startFrameValue.toString(), projectStartFrame.getAttribute("innerText"));
        CustomAssert.assertEquals("Project End Frame Test", endFrameValue.toString(), projectEndFrame.getAttribute("innerText"));
        CustomAssert.assertEquals("Project Frame Step Test", frameStepValue.toString(), projectFrameStep.getAttribute("innerText"));
        CustomAssert.assertEquals("Scaling Value Test", scalingValue + "%", projectResPercentage.getAttribute("innerText"));
        CustomAssert.assertEquals("Resolution Test", xValue + " x " + yValue, projectResolution.getAttribute("innerText"));
        CustomAssert.assertEquals("Compute Type Test", compute, projectComputeType.getAttribute("innerText"));
        CustomAssert.assertEquals("Render Engine Test", engine, projectBlenderEngine.getAttribute("innerText"));
        CustomAssert.assertEquals("Project Type Test", "Animation", projectType.getAttribute("innerText"));
        CustomAssert.assertEquals("Project Name Test", sampleProjectName, projectName.getAttribute("innerText"));
        CustomAssert.assertEquals("Project Render Output Test", selectedOutputFormat, projectRenderOutputFormat.getAttribute("innerText"));

        if (engineOptions == EngineOptions.CYCLES) {
            CustomAssert.assertEquals("Number of Samples Test", numberOfSamples, projectSamples.getAttribute("innerText"));
        }

        // Verify Project List
        Utils.commentGenerator("Returning to Project List");
        WebElement projectReturn = driver.findElement(By.id("project_list"));
        projectReturn.click();
        Thread.sleep(1000);
        if (deleteProject) {
            currentUrl = driver.getCurrentUrl();
            CustomAssert.assertEquals("Project List URL Test", host + "project", currentUrl);
            projectName = driver.findElement(By.xpath("//*[contains(@id, 'projectName')]"));
            projectType = driver.findElement(By.xpath("//*[contains(@id, 'projectType')]"));
            WebElement projectDelete = driver.findElement(By.xpath("//*[contains(@id, 'projectDelete')]"));
            projectComputeType = driver.findElement(By.xpath("//*[contains(@id, 'projectComputeType')]"));
            projectResolution = driver.findElement(By.xpath("//*[contains(@id, 'projectResolution')]"));

            // Project List Assertions
            Utils.commentGenerator("Project List Assertions");
            CustomAssert.assertEquals("Project List - Resolution Test", xValue + " x " + yValue,
                    projectResolution.getAttribute("innerText"));
            CustomAssert.assertEquals("Project List - Compute Type Test", compute,
                    projectComputeType.getAttribute("innerText"));
            CustomAssert.assertEquals("Project List - Project Type Test", "Animation",
                    projectType.getAttribute("innerText"));
            CustomAssert.assertEquals("Project List - Project Name Test", sampleProjectName, projectName.getAttribute("innerText"));
            Thread.sleep(1000);

            deleteProject(projectDelete);
        }

        return projectId;
    }

    private void deleteProject(WebElement projectDelete) throws InterruptedException {
        Utils.commentGenerator("Deleting Project");
        projectDelete.click();
        Thread.sleep(1000);
    }

    public void createMultipleProjects(WebDriver driver, String host, WebDriverWait wait, List<String> multipleFiles,
                                       int numofLoops, EngineOptions engineOptions) throws InterruptedException {

        Utils.commentGenerator("Creating " + numofLoops + " " + engineOptions.name() + " Projects");
        for (int i = 0; i < numofLoops; i++) {
            Utils.commentGenerator("Project Creation Loop: " + (i + 1) + " of " + numofLoops);
            Random r = new Random();
            List<ComputeOptions> computeOptionsList = new ArrayList<>();
            computeOptionsList.add(ComputeOptions.CPU);
            computeOptionsList.add(ComputeOptions.GPU);
            int count = multipleFiles.size();
            int value = r.nextInt(count);
            int computeRandom = r.nextInt(computeOptionsList.size());
            Integer resolutionX = r.nextInt(9999 - 500) + 500;
            Integer resolutionY = r.nextInt(8000 - 200) + 200;
            Integer samples = r.nextInt(300 - 20) + 20;
            if (r.nextBoolean()) {
                createStillImageProject(driver, host, wait, multipleFiles.get(value), computeOptionsList.get(computeRandom), engineOptions,
                        false, resolutionX.toString(), resolutionY.toString(), false,
                        samples.toString(), false, false);
            } else {
                createAnimationProject(driver, host, wait, multipleFiles.get(value), computeOptionsList.get(computeRandom), engineOptions,
                        false, resolutionX.toString(), resolutionY.toString(), false,
                        samples.toString(), true, null, null, null,
                        true, null, false, false);
            }
            Thread.sleep(1000);
        }
        for (int i = 0; i < numofLoops; i++) {
            Utils.commentGenerator("Project Deletion Loop: " + (i + 1) + " of " + numofLoops);
            Thread.sleep(1000);
            WebElement projectDelete = driver.findElement(By.xpath("//*[contains(@id, 'projectDelete')]"));
            projectDelete.click();
        }


    }

    public void emptyProjectName(WebDriver driver, String host) throws InterruptedException {
        String filename = "bmw27_cpu.blend";
        Utils.commentGenerator("Starting Empty Project Name Test");
        Thread.sleep(1000);
        WebElement menuItemProject = driver.findElement(By.id("projects"));
        menuItemProject.click();
        Utils.commentGenerator("Loading Project List");
        Thread.sleep(2000);
        String currentUrl = driver.getCurrentUrl();
        CustomAssert.assertEquals("Project List URL", host + "project", currentUrl);
        WebElement newProjectBtn = driver.findElement(By.id("project_new"));
        Thread.sleep(2000);
        newProjectBtn.click();
        Utils.commentGenerator("Starting New Project Wizard");
        Thread.sleep(2000);
        currentUrl = driver.getCurrentUrl();
        CustomAssert.assertEquals("New Project URL", host + "project/new", currentUrl);

        // Project File Upload Form
        Utils.commentGenerator("Project File Upload Form");
        WebElement projectForm = driver.findElement(By.id("projectWizard"));
        WebElement projectUpload = driver.findElement(By.id("projectUpload"));
        String currentPath = Paths.get(".").toAbsolutePath().normalize().toString();
        String projectFile = currentPath + "/sethlans-files/blend_files/" + filename;
        Utils.commentGenerator("Current file to test " + projectFile);
        projectUpload.sendKeys(projectFile);
        projectForm.submit();
        Thread.sleep(5000);

        // Project Details Form
        Utils.commentGenerator("Project Details Form");
        WebElement projectFormTitle = driver.findElement(By.id("projectFormTitle"));
        CustomAssert.assertEquals("Project Details Heading", "Project Details",
                projectFormTitle.getAttribute("innerText"));
        WebElement projectSubmit = driver.findElement(By.id("projectSubmit"));
        projectSubmit.click();
        WebElement alert = driver.findElement(By.xpath("//div[contains(.,'Please correct the errors')]"));
        WebElement projectNameEmpty = driver.findElement(By.xpath("//li[contains(.,'may not be empty')]"));
        CustomAssert.assertNotNull("Alert Banner Displayed", alert);
        CustomAssert.assertNotNull("Notification of Empty Project Name Displayed", projectNameEmpty);

    }

    public void shortProjectName(WebDriver driver, String host) throws InterruptedException {
        String filename = "bmw27_cpu.blend";
        Utils.commentGenerator("Starting Short Project Name Test");
        Thread.sleep(1000);
        WebElement menuItemProject = driver.findElement(By.id("projects"));
        menuItemProject.click();
        Utils.commentGenerator("Loading Project List");
        Thread.sleep(2000);
        String currentUrl = driver.getCurrentUrl();
        CustomAssert.assertEquals("Project List URL", host + "project", currentUrl);
        WebElement newProjectBtn = driver.findElement(By.id("project_new"));
        Thread.sleep(2000);
        newProjectBtn.click();
        Utils.commentGenerator("Starting New Project Wizard");
        Thread.sleep(2000);
        currentUrl = driver.getCurrentUrl();
        CustomAssert.assertEquals("New Project URL", host + "project/new", currentUrl);

        // Project File Upload Form
        Utils.commentGenerator("Project File Upload Form");
        WebElement projectForm = driver.findElement(By.id("projectWizard"));
        WebElement projectUpload = driver.findElement(By.id("projectUpload"));
        String currentPath = Paths.get(".").toAbsolutePath().normalize().toString();
        String projectFile = currentPath + "/sethlans-files/blend_files/" + filename;
        Utils.commentGenerator("Current file to test " + projectFile);
        projectUpload.sendKeys(projectFile);
        projectForm.submit();
        Thread.sleep(5000);

        // Project Details Form
        Utils.commentGenerator("Project Details Form");
        WebElement projectFormTitle = driver.findElement(By.id("projectFormTitle"));
        CustomAssert.assertEquals("Project Details Heading", "Project Details",
                projectFormTitle.getAttribute("innerText"));
        // Enter Project Name
        WebElement projectName = driver.findElement(By.id("projectName"));
        String sampleProjectName = "cat";
        Utils.slowCharEntry(projectName, sampleProjectName);
        WebElement projectSubmit = driver.findElement(By.id("projectSubmit"));
        projectSubmit.click();
        WebElement alert = driver.findElement(By.xpath("//div[contains(.,'Please correct the errors')]"));
        WebElement projectNameMinSize = driver.findElement(By.xpath("//li[contains(.,'size must be between 4')]"));
        CustomAssert.assertNotNull("Alert Banner Displayed", alert);
        CustomAssert.assertNotNull("Notification of Minimum Project Name Size Displayed", projectNameMinSize);

    }

}
