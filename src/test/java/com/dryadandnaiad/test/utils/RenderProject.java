package com.dryadandnaiad.test.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created Mario Estrella on 1/11/18.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration
 */
public class RenderProject {

    public void startRender(WebDriver driver, int projectId) throws InterruptedException {
        // Starting Render
        WebElement projectStatus = driver.findElement(By.id("projectStatus-" + projectId));
        CustomAssert.assertEquals("Verify Project Not Started", "Not Started", projectStatus.getAttribute("innerText"));
        WebElement startRender = driver.findElement(By.id("projectStartLink-" + projectId));
        startRender.click();
        Thread.sleep(3000);
        projectStatus = driver.findElement(By.id("projectStatus-" + projectId));
        CustomAssert.assertNotEquals("Verify Project Started", "Not Started", projectStatus.getAttribute("innerText"));
        Thread.sleep(3000);
        WebElement projectView = driver.findElement(By.id("projectViewLink-" + projectId));
        projectView.click();
        WebElement projectStatusView;
        while (true) {
            driver.navigate().refresh();
            projectStatusView = driver.findElement(By.id("projectStatus"));
            if (projectStatusView.getAttribute("innerText").contains("Rendering")) {
                WebElement currentPercentage = driver.findElement(By.id("currentPercentage"));
                Utils.commentGenerator("Current Rendering Percentage: " + currentPercentage.getAttribute("innerText"));
                Thread.sleep(10000);
            }
            if (projectStatusView.getAttribute("innerText").contains("Complete")) {
                WebElement currentPercentage = driver.findElement(By.id("currentPercentage"));
                Utils.commentGenerator("Current Rendering Percentage: " + currentPercentage.getAttribute("innerText"));
                break;
            }
        }
        projectStatusView = driver.findElement(By.id("projectStatus"));
        CustomAssert.assertEquals("Render Status Complete", "Status: Complete", projectStatusView.getAttribute("innerText"));
        WebElement projectList = driver.findElement(By.id("project_list"));
        projectList.click();
        Thread.sleep(2000);
    }
}
