package com.dryadandnaiad.test.utils;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created Mario Estrella on 12/24/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class CheckBenchmarks {

    public static void checkBenchmark(ComputeOptions computeOptions, WebDriverWait wait, WebDriver driver) throws InterruptedException {
        WebElement nodeBenchmark;
        long startTime = System.currentTimeMillis();

        switch (computeOptions) {
            case CPU:
                while (true) {
                    driver.navigate().refresh();
                    WebElement nodesLink = driver.findElement(By.id("settings_nodes"));
                    nodesLink.click();
                    Thread.sleep(2000);
                    long currentTime = System.currentTimeMillis();
                    try {
                        nodeBenchmark = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("td[id*='nodeBenchmarkRating']")));

                        if (nodeBenchmark.getAttribute("innerText").equals("N/A")) {
                            Utils.commentGenerator("Waiting for CPU benchmark. Sleeping for 1 minute.");
                            Thread.sleep(60000);
                        } else if (nodeBenchmark.getAttribute("innerText").contains("CPU")) {
                            CustomAssert.assertTrue("CPU Benchmark complete", true);
                            Utils.commentGenerator("Benchmark Rating: \n" + nodeBenchmark.getAttribute("innerText"));
                            break;
                        }
                    } catch (StaleElementReferenceException e) {
                        Utils.commentGenerator("Stale element found, waiting for next loop.");
                    } catch (TimeoutException ex) {
                        Utils.commentGenerator("Timed out waiting for nodeBenchmark, waiting until next loop.");
                    }
                    long totalTime = currentTime - startTime;
                    if (totalTime > 1200000) {
                        CustomAssert.assertTrue("CPU Benchmark Failed.", false);
                        Utils.commentGenerator("Test timed out. Over 20 minutes.");
                        break;
                    }
                }
                break;
            case GPU:
                while (true) {
                    driver.navigate().refresh();
                    WebElement nodesLink = driver.findElement(By.id("settings_nodes"));
                    nodesLink.click();
                    Thread.sleep(2000);
                    long currentTime = System.currentTimeMillis();
                    try {
                        nodeBenchmark = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("td[id*='nodeBenchmarkRating']")));

                        if (nodeBenchmark.getAttribute("innerText").equals("N/A")) {
                            Utils.commentGenerator("Waiting for GPU benchmark. Sleeping for 1 minute.");
                            Thread.sleep(60000);
                        } else if (nodeBenchmark.getAttribute("innerText").contains("CUDA")) {
                            CustomAssert.assertTrue("GPU Benchmark complete", true);
                            Utils.commentGenerator("Benchmark Rating: \n" + nodeBenchmark.getAttribute("innerText"));
                            break;
                        }
                    } catch (StaleElementReferenceException e) {
                        Utils.commentGenerator("Stale element found, waiting for next loop.");
                    } catch (TimeoutException ex) {
                        Utils.commentGenerator("Timed out waiting for nodeBenchmark, waiting until next loop.");
                    }
                    long totalTime = currentTime - startTime;
                    if (totalTime > 1200000) {
                        CustomAssert.assertTrue("GPU Benchmark Failed", false);
                        Utils.commentGenerator("Test timed out. Over 20 minutes.");
                        break;
                    }
                }
                break;
            case CPU_GPU:
                while (true) {
                    driver.navigate().refresh();
                    WebElement nodesLink = driver.findElement(By.id("settings_nodes"));
                    nodesLink.click();
                    Thread.sleep(2000);
                    long currentTime = System.currentTimeMillis();
                    try {
                        nodeBenchmark = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("td[id*='nodeBenchmarkRating']")));

                        if (nodeBenchmark.getAttribute("innerText").equals("N/A")) {
                            Utils.commentGenerator("Waiting for CPU and GPU benchmarks. Sleeping for 1 minute.");
                            Thread.sleep(60000);
                        } else if (nodeBenchmark.getAttribute("innerText").contains("CPU") && nodeBenchmark.getAttribute("innerText").contains("CUDA")) {
                            CustomAssert.assertTrue("CPU and GPU Benchmarks complete", true);
                            Utils.commentGenerator("Benchmark Rating: \n" + nodeBenchmark.getAttribute("innerText"));
                            break;
                        }
                    } catch (StaleElementReferenceException e) {
                        Utils.commentGenerator("Stale element found, waiting for next loop.");
                    } catch (TimeoutException ex) {
                        Utils.commentGenerator("Timed out waiting for nodeBenchmark, waiting until next loop.");
                    }
                    long totalTime = currentTime - startTime;
                    if (totalTime > 1200000) {
                        CustomAssert.assertTrue("CPU and GPU Benchmarks Failed", false);
                        Utils.commentGenerator("Test timed out. Over 20 minutes.");
                        break;
                    }
                }

        }
    }

    public static void multiNodeCheckBenchmark(ComputeOptions computeOptions, WebDriverWait wait, WebDriver driver) throws InterruptedException {
        List<WebElement> nodeBenchmarks;
        long startTime = System.currentTimeMillis();
        int benchCount;



        switch (computeOptions) {
            case CPU:
                while (true) {
                    driver.navigate().refresh();
                    Thread.sleep(2000);
                    WebElement nodesLink = driver.findElement(By.id("settings_nodes"));
                    nodesLink.click();
                    long currentTime = System.currentTimeMillis();
                    try {
                        nodeBenchmarks = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("td[id*='nodeBenchmarkRating']")));
                        benchCount = nodeBenchmarks.size();


                        for (WebElement nodeBenchmark : nodeBenchmarks) {
                            if (nodeBenchmark.getAttribute("innerText").equals("N/A")) {
                                Utils.commentGenerator("Waiting for CPU benchmark. Sleeping for 1 minute.");
                                Thread.sleep(60000);
                            } else if (nodeBenchmark.getAttribute("innerText").contains("CPU")) {
                                CustomAssert.assertTrue("CPU Benchmark complete", true);
                                Utils.commentGenerator("Benchmark Rating: \n" + nodeBenchmark.getAttribute("innerText"));
                                benchCount--;
                            }
                        }

                        Utils.commentGenerator("Remaining benchmarks " + benchCount);
                        if (benchCount <= 0) {
                            break;
                        }
                    } catch (StaleElementReferenceException e) {
                        Utils.commentGenerator("Stale element found, waiting for next loop.");
                    } catch (TimeoutException ex) {
                        Utils.commentGenerator("Timed out waiting for nodeBenchmark, waiting until next loop.");
                    }

                    long totalTime = currentTime - startTime;
                    if (totalTime > 1200000) {
                        CustomAssert.assertTrue("CPU Benchmark Failed.", false);
                        Utils.commentGenerator("Test timed out. Over 20 minutes.");
                        break;
                    }
                }
                break;
            case GPU:
                while (true) {
                    driver.navigate().refresh();
                    Thread.sleep(2000);
                    WebElement nodesLink = driver.findElement(By.id("settings_nodes"));
                    nodesLink.click();
                    long currentTime = System.currentTimeMillis();
                    try {
                        nodeBenchmarks = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("td[id*='nodeBenchmarkRating']")));
                        benchCount = nodeBenchmarks.size();

                        for (WebElement nodeBenchmark : nodeBenchmarks) {
                            if (nodeBenchmark.getAttribute("innerText").equals("N/A")) {
                                Utils.commentGenerator("Waiting for GPU benchmark. Sleeping for 1 minute.");
                                Thread.sleep(60000);
                            } else if (nodeBenchmark.getAttribute("innerText").contains("GPU")) {
                                CustomAssert.assertTrue("GPU Benchmark complete", true);
                                Utils.commentGenerator("Benchmark Rating: \n" + nodeBenchmark.getAttribute("innerText"));
                                benchCount--;
                                break;
                            }


                        }


                        Utils.commentGenerator("Remaining benchmarks " + benchCount);
                        if (benchCount <= 0) {
                            break;
                        }
                    } catch (StaleElementReferenceException e) {
                        Utils.commentGenerator("Stale element found, waiting for next loop.");
                    } catch (TimeoutException ex) {
                        Utils.commentGenerator("Timed out waiting for nodeBenchmark, waiting until next loop.");
                    }

                    long totalTime = currentTime - startTime;
                    if (totalTime > 1200000) {
                        CustomAssert.assertTrue("GPU Benchmark Failed", false);
                        Utils.commentGenerator("Test timed out. Over 20 minutes.");
                        break;
                    }
                }
                break;
            case CPU_GPU:
                while (true) {
                    driver.navigate().refresh();
                    Thread.sleep(2000);
                    WebElement nodesLink = driver.findElement(By.id("settings_nodes"));
                    nodesLink.click();
                    long currentTime = System.currentTimeMillis();
                    try {
                        nodeBenchmarks = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("td[id*='nodeBenchmarkRating']")));
                        benchCount = nodeBenchmarks.size();

                        for (WebElement nodeBenchmark : nodeBenchmarks) {

                            if (nodeBenchmark.getAttribute("innerText").equals("N/A")) {
                                Utils.commentGenerator("Waiting for CPU and GPU benchmarks. Sleeping for 1 minute.");
                                Thread.sleep(60000);
                            } else if (nodeBenchmark.getAttribute("innerText").contains("CPU") && nodeBenchmark.getAttribute("innerText").contains("CUDA")) {
                                CustomAssert.assertTrue("CPU and GPU Benchmarks complete", true);
                                Utils.commentGenerator("Benchmark Rating: \n" + nodeBenchmark.getAttribute("innerText"));
                                benchCount--;
                                break;
                            }

                        }

                        Utils.commentGenerator("Remaining benchmarks " + benchCount);
                        if (benchCount <= 0) {
                            break;
                        }
                    } catch (StaleElementReferenceException e) {
                        Utils.commentGenerator("Stale element found, waiting for next loop.");
                    } catch (TimeoutException ex) {
                        Utils.commentGenerator("Timed out waiting for nodeBenchmark, waiting until next loop.");
                    }

                    long totalTime = currentTime - startTime;
                    if (totalTime > 1200000) {
                        CustomAssert.assertTrue("CPU and GPU Benchmarks Failed", false);
                        Utils.commentGenerator("Test timed out. Over 20 minutes.");
                        break;
                    }
                }

        }
    }

}
