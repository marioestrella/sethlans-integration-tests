package com.dryadandnaiad.test.utils;

import org.junit.Assert;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created Mario Estrella on 11/8/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class CustomAssert {

    public static void assertNotNull(String description, Object object) {
        try {
            Assert.assertNotNull(description, object);
            assertCommentGenerator(description, true);
        } catch (AssertionError e) {
            assertCommentGenerator(description, false);
            assertErrorMessageGenerator(description, e);
            throw e;
        }
    }

    public static void assertEquals(String description, Object expected, Object actual) {
        try {
            Assert.assertEquals(description, expected, actual);
            assertCommentGenerator(description, true);
        } catch (AssertionError e) {
            assertCommentGenerator(description, false);
            assertErrorMessageGenerator(description, e);
            throw e;
        }
    }

    public static void assertNotEquals(String description, Object expected, Object actual) {
        try {
            Assert.assertNotEquals(description, expected, actual);
            assertCommentGenerator(description, true);
        } catch (AssertionError e) {
            assertCommentGenerator(description, false);
            assertErrorMessageGenerator(description, e);
            throw e;
        }
    }

    public static void assertTrue(String description, boolean object) {
        try {
            Assert.assertTrue(description, object);
            assertCommentGenerator(description, true);
        } catch (AssertionError e) {
            assertCommentGenerator(description, false);
            assertErrorMessageGenerator(description, e);
            throw e;
        }
    }

    public static <T> void assertThat(String description, T actual, org.hamcrest.Matcher<T> matcher) {
        try {
            Assert.assertThat(description, actual, matcher);
            assertCommentGenerator(description, true);
        } catch (AssertionError e) {
            assertCommentGenerator(description, false);
            assertErrorMessageGenerator(description, e);
            throw e;
        }
    }

    private static void assertCommentGenerator(String comment, boolean pass) {
        if (pass) {
            System.out.println(timeStamp() + ": RESULT - " + comment + " - PASSED " + System.lineSeparator());
        } else {
            System.out.println(timeStamp() + ": RESULT - " + comment + " - FAILED " + System.lineSeparator());
        }
    }

    private static void assertErrorMessageGenerator(String description, AssertionError error) {
        System.out.println(timeStamp() + ": FAILURE - " + error.getMessage() + System.lineSeparator());
    }

    private static String timeStamp() {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        Date today = Calendar.getInstance().getTime();
        String date = df.format(today);
        return date;
    }
}
