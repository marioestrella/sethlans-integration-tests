package com.dryadandnaiad.test.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created Mario Estrella on 12/24/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class ServerVerifyBenchmarks {

    public void verifyBenchmarks(WebDriver driver, List<String> nodeLists, ComputeOptions computeOptions, WebDriverWait wait) throws InterruptedException {
        WebElement goHome = driver.findElement(By.id("home"));
        goHome.click();
        Thread.sleep(1000);
        WebElement menuButton = driver.findElement(By.id("settings"));
        menuButton.click();
        Utils.commentGenerator("Loading Settings");
        Thread.sleep(2000);
        WebElement nodesLink = driver.findElement(By.id("settings_nodes"));
        nodesLink.click();
        Thread.sleep(1000);
        List<WebElement> nodeHostname = driver.findElements(By.cssSelector("td[id*='nodeHostname']"));
        List<WebElement> nodeStatus = driver.findElements(By.cssSelector("td[id*='nodeStatus']"));
        for (WebElement webElement : nodeHostname) {
            for (String node : nodeLists) {
                int iend = node.indexOf(".");
                if (iend != -1) {
                    Utils.commentGenerator(node + " contains a domain name. Removing it.");
                    node = node.substring(0, iend);
                }
                if (webElement.getAttribute("innerText").contains(node.toUpperCase())) {
                    CustomAssert.assertEquals("Node Verification Test for " + node.toUpperCase(), node.toUpperCase(), webElement.getAttribute("innerText"));
                }


            }
        }
        Thread.sleep(1000);
        for (WebElement status : nodeStatus) {
            for (String node : nodeLists) {
                int iend = node.indexOf(".");
                if (iend != -1) {
                    Utils.commentGenerator(node + " contains a domain name. Removing it.");
                    node = node.substring(0, iend);
                }
                CustomAssert.assertNotEquals("Node Status Test for " + node.toUpperCase(), "Pending Activation", status.getAttribute("innerText"));

            }
        }
        Thread.sleep(1000);


        CheckBenchmarks.multiNodeCheckBenchmark(computeOptions, wait, driver);

    }
}
