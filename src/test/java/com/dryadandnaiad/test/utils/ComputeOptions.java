package com.dryadandnaiad.test.utils;

/**
 * Created Mario Estrella on 9/21/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public enum ComputeOptions {
    CPU, GPU, CPU_GPU
}
