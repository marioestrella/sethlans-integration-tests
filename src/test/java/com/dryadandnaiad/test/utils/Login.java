package com.dryadandnaiad.test.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created Mario Estrella on 9/21/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class Login {

    public void sethlansLogin(String usernameString, String passwordString, WebDriverWait wait, WebDriver driver) throws InterruptedException {
        if (!driver.getCurrentUrl().contains("login")) {
            Utils.commentGenerator("Refresh to get Login Screen");
            driver.navigate().refresh();
        }
        Utils.commentGenerator("Starting Login");
        Thread.sleep(3000);
        WebElement username = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        WebElement password = driver.findElement(By.id("password"));
        Utils.commentGenerator("Entering username");
        Utils.slowCharEntry(username, usernameString);
        Thread.sleep(1000);
        Utils.commentGenerator("Entering password");
        Utils.slowCharEntry(password, passwordString);
        Thread.sleep(1000);
        Utils.commentGenerator("Submitting Login");
        clickLoginButton(wait);
        Thread.sleep(4000);
    }

    private void clickLoginButton(WebDriverWait wait) {
        WebElement loginButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("loginButton")));
        loginButton.click();
    }
}
