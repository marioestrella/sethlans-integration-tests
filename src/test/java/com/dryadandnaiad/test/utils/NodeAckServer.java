package com.dryadandnaiad.test.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created Mario Estrella on 12/24/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class NodeAckServer {

    public void startServerAcknowledgement(WebDriver driver, String clientHostname, Integer validPort, String username, String password, String serverHost,
                                           ComputeOptions computeOptions, WebDriverWait wait) throws InterruptedException {
        Utils.commentGenerator("Navigating to " + clientHostname);
        driver.get(clientHostname);
        Login login = new Login();
        login.sethlansLogin(username, password, wait, driver);
        Thread.sleep(3000);
        if (computeOptions.equals(ComputeOptions.CPU) || computeOptions.equals(ComputeOptions.CPU_GPU)) {
            WebElement menuButton = driver.findElement(By.id("settings"));
            menuButton.click();
            Utils.commentGenerator("Loading Settings");
            Thread.sleep(1000);
            WebElement computeSettings = driver.findElement(By.id("settings_compute"));
            computeSettings.click();
            Thread.sleep(1000);
            WebElement cpuGroup = driver.findElement(By.id("cpu"));
            Utils.selectMaxSliderValue(driver, cpuGroup);
            WebElement updateButton = driver.findElement(By.id("update_method"));
            updateButton.click();
            Thread.sleep(35000);
            login = new Login();
            login.sethlansLogin(username, password, wait, driver);
            Thread.sleep(2000);
        }

        Thread.sleep(2000);
        WebElement menuButton = driver.findElement(By.id("settings"));
        menuButton.click();
        Utils.commentGenerator("Loading Settings");
        Thread.sleep(2000);
        //Server Requests
        WebElement servers = driver.findElement(By.id("settings_servers"));
        servers.click();
        Utils.commentGenerator("Navigating to Server List");
        Thread.sleep(1000);
        driver.navigate().refresh();
        Thread.sleep(1000);
        Utils.commentGenerator("Checking server status on " + clientHostname);

        // Server List Verification
        WebElement serverStatus = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("td[id*='serverStatus']")));
        WebElement serverHostname = driver.findElement(By.cssSelector("td[id*='serverHostname']"));
        WebElement serverNetworkPort = driver.findElement(By.cssSelector("td[id*='serverNetworkPort']"));
        WebElement serverAcknowledgement = driver.findElement(By.cssSelector("a[id*='serverAcknowledgement']"));


        //Server List Assertions
        CustomAssert.assertEquals("Server List - Status Test",
                "Pending Acknowledgement", serverStatus.getAttribute("innerText"));
        CustomAssert.assertEquals("Server List - Hostname Test",
                serverHost, serverHostname.getAttribute("innerText"));
        CustomAssert.assertEquals("Server List - Network Port Test",
                validPort.toString(), serverNetworkPort.getAttribute("innerText"));

        // Acknowledgement of Server
        serverAcknowledgement.click();
        Thread.sleep(2000);
        serverStatus = driver.findElement(By.cssSelector("td[id*='serverStatus']"));
        CustomAssert.assertEquals("Server List - Pending Server Response Status Test",
                "Pending Server Response", serverStatus.getAttribute("innerText"));
        servers = driver.findElement(By.id("settings_servers"));
        servers.click();
        Thread.sleep(2000);
        serverStatus = driver.findElement(By.cssSelector("td[id*='serverStatus']"));
        CustomAssert.assertEquals("Server List - Active Status Test",
                "Active", serverStatus.getAttribute("innerText"));
        Thread.sleep(2000);
    }
}
