package com.dryadandnaiad.test.utils;

/**
 * Created Mario Estrella on 10/25/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public enum EngineOptions {
    BLENDER_RENDER, CYCLES
}
