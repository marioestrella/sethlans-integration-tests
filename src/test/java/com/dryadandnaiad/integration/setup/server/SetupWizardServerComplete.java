package com.dryadandnaiad.integration.setup.server;

import com.dryadandnaiad.integration.setup.AbstractSetupWizardComplete;
import com.dryadandnaiad.test.utils.CustomAssert;
import com.dryadandnaiad.test.utils.Login;
import com.dryadandnaiad.test.utils.ServerSetup;
import com.dryadandnaiad.test.utils.Utils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SetupWizardServerComplete extends AbstractSetupWizardComplete {

    @BeforeClass
    public static void serverSetup() throws InterruptedException {
        ServerSetup serverSetup = new ServerSetup();
        serverSetup.startSetup(wait, driver, setupHost, sethlansOS, usernameString, passwordString, validPort);
    }


    @Test
    public void test_server_setup_complete() throws InterruptedException {
        setupCompleteTest("Server");
    }

    @Test
    public void test_server_menu() throws InterruptedException {
        Utils.commentGenerator("Testing Server Menu");
        Utils.commentGenerator("Navigating to " + host + "home");
        driver.get(host + "home");
        Login login = new Login();
        login.sethlansLogin(usernameString, passwordString, wait, driver);
        WebElement menuButton = driver.findElement(By.id("projects"));
        menuButton.click();
        Utils.commentGenerator("Navigating to Project Page");
        String currentURL = driver.getCurrentUrl();
        CustomAssert.assertEquals("Project Page Displayed", host + "project", currentURL);
    }


}
