package com.dryadandnaiad.integration.setup.setupwizard;

import com.dryadandnaiad.test.utils.CustomAssert;
import com.dryadandnaiad.test.utils.Utils;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;

/**
 * Created Mario Estrella on 9/5/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class DualSetupTest {

    private static WebDriver driver;
    private String usernameString;
    private String passwordString;
    private Integer validPort;
    private DesiredCapabilities capabilities = new DesiredCapabilities();
    private WebElement nextButton = null;
    private WebDriverWait wait;
    private Select cpuDropdown;
    private WebElement selected;
    private WebElement cpuGroup;
    private WebElement gpuGroup;
    private WebElement gpuCheckbox;
    private WebElement modeSummary;
    private WebElement computeSummary;
    private Select blenderDropdown;
    private WebElement versionSummary;
    private WebElement usernameSummary;
    private WebElement portSummary;
    private String sethlansOS = System.getProperty("os").toLowerCase();
    private String host;


    @Before
    public void setup() throws InterruptedException {
        Utils.commentGenerator("Setup Wizard Dual Config Tests");
        host = "https://" + sethlansOS + ":7443";
        capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        Utils.setDriver();
        driver = new FirefoxDriver(capabilities);
        validPort = Utils.validPortGenerator();
        usernameString = Utils.usernameGenerator(20);
        passwordString = Utils.passwordGenerator(12);
        Thread.sleep(3000);
    }

    private void bothScreen() {
        wait = new WebDriverWait(driver, 20);
        Utils.commentGenerator("Navigating to " + host);
        driver.get(host);
        WebElement username = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        WebElement password = driver.findElement(By.id("password"));
        try {
            Utils.slowCharEntry(username, usernameString);
            Utils.slowCharEntry(password, passwordString);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement passwordConfirm = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("passwordConfirm")));
        try {
            Utils.slowCharEntry(passwordConfirm, passwordString);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement port = driver.findElement(By.id("httpsPort"));
        port.clear();
        port.sendKeys(validPort.toString());
        Utils.setupClickNextButton(nextButton, wait);
        WebElement nodeButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mode3")));
        nodeButton.click();
        Utils.setupClickNextButton(nextButton, wait);

    }

    @Test
    public void test_both_cpu_mode() {
        Utils.commentGenerator("Dual CPU Mode Test");
        bothScreen();
        Random r = new Random();
        cpuDropdown = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("selectedMethod"))));
        cpuDropdown.selectByValue("CPU");
        WebElement coreSlider = driver.findElement(By.className("slider-handle"));
        Utils.moveSlider(driver, coreSlider);
        WebElement cores = driver.findElement(By.id("cores"));
        String numberofCores = cores.getAttribute("value");
        blenderDropdown = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("blenderVersion"))));
        int count = blenderDropdown.getOptions().size();
        int index = r.nextInt(count);
        blenderDropdown.selectByIndex(index);
        selected = blenderDropdown.getFirstSelectedOption();
        String blenderVersion = selected.getAttribute("innerText");
        Utils.setupClickNextButton(nextButton, wait);
        versionSummary = driver.findElement(By.xpath("//td[contains(.,'" + blenderVersion + "')]"));
        modeSummary = driver.findElement(By.xpath("//tr[contains(.,'Mode')]"));
        computeSummary = driver.findElement(By.xpath("//tr[contains(.,'Compute Method')]"));
        WebElement coreSummary = driver.findElement(By.xpath("//tr[contains(.,'Rendering Cores')]"));
        usernameSummary = driver.findElement(By.xpath("//tr[contains(.,'User Name')]"));
        portSummary = driver.findElement(By.xpath("//tr[contains(.,'HTTPS Port')]"));
        CustomAssert.assertThat("CPU Core Summary Match", coreSummary.getAttribute("innerText"),
                CoreMatchers.containsString(numberofCores));
        CustomAssert.assertThat("CPU Compute Summary Match", computeSummary.getAttribute("innerText"),
                CoreMatchers.containsString("CPU"));
        CustomAssert.assertThat("DUAL Summary Match", modeSummary.getAttribute("innerText"),
                CoreMatchers.containsString("Dual"));
        CustomAssert.assertThat("Blender Version Summary Match", versionSummary.getAttribute("innerText"),
                CoreMatchers.containsString(blenderVersion));
        CustomAssert.assertThat("Username Match", usernameSummary.getAttribute("innerText"),
                CoreMatchers.containsString(usernameString.toLowerCase()));
        CustomAssert.assertThat("Port Sumamry Match", portSummary.getAttribute("innerText"),
                CoreMatchers.containsString(validPort.toString()));
    }

    @Test
    public void test_both_single_gpu_mode() {
        Utils.commentGenerator("Dual GPU Mode Test");
        bothScreen();
        cpuDropdown = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("selectedMethod"))));
        if (cpuDropdown.getOptions().size() > 1) {
            cpuDropdown.selectByValue("GPU");
            Random r = new Random();
            blenderDropdown = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("blenderVersion"))));
            int count = blenderDropdown.getOptions().size();
            int index = r.nextInt(count);
            blenderDropdown.selectByIndex(index);
            selected = blenderDropdown.getFirstSelectedOption();
            String blenderVersion = selected.getAttribute("innerText");
            gpuGroup = driver.findElement(By.id("cuda"));
            cpuGroup = driver.findElement(By.id("cpu"));
            CustomAssert.assertTrue("GPU Group Displayed", gpuGroup.isDisplayed());
            CustomAssert.assertTrue("CPU Group Not Displayed", !cpuGroup.isDisplayed());
            gpuCheckbox = driver.findElement(By.id("0"));
            WebElement gpuLabel = driver.findElement(By.id("0-label"));
            String gpuName = gpuLabel.getAttribute("innerText");
            CustomAssert.assertTrue("No GPU is pre-selected", !gpuCheckbox.isSelected());
            gpuCheckbox.click();
            Utils.setupClickNextButton(nextButton, wait);
            modeSummary = driver.findElement(By.xpath("//tr[contains(.,'Mode')]"));
            computeSummary = driver.findElement(By.xpath("//tr[contains(.,'Compute Method')]"));
            WebElement gpuSummary = driver.findElement(By.xpath("//tr[contains(.,'Selected GPU')]"));
            versionSummary = driver.findElement(By.xpath("//td[contains(.,'" + blenderVersion + "')]"));
            usernameSummary = driver.findElement(By.xpath("//tr[contains(.,'User Name')]"));
            portSummary = driver.findElement(By.xpath("//tr[contains(.,'HTTPS Port')]"));
            CustomAssert.assertThat("DUAL Summary Match", modeSummary.getAttribute("innerText"),
                    CoreMatchers.containsString("Dual"));
            CustomAssert.assertThat("GPU Compute Match", computeSummary.getAttribute("innerText"),
                    CoreMatchers.containsString("GPU"));
            CustomAssert.assertThat("GPU Graphics Card Match", gpuSummary.getAttribute("innerText"),
                    CoreMatchers.containsString(gpuName));
            CustomAssert.assertThat("Blender Version Summary Match", versionSummary.getAttribute("innerText"),
                    CoreMatchers.containsString(blenderVersion));
            CustomAssert.assertThat("Username Match", usernameSummary.getAttribute("innerText"),
                    CoreMatchers.containsString(usernameString.toLowerCase()));
            CustomAssert.assertThat("Port Sumamry Match", portSummary.getAttribute("innerText"),
                    CoreMatchers.containsString(validPort.toString()));
        }
    }

    @Test
    public void test_both_no_gpu_selected_gpu_mode() {
        Utils.commentGenerator("No GPU Selected in GPU Mode");
        bothScreen();
        cpuDropdown = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("selectedMethod"))));
        if (cpuDropdown.getOptions().size() > 1) {
            Random r = new Random();
            blenderDropdown = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("blenderVersion"))));
            int count = blenderDropdown.getOptions().size();
            int index = r.nextInt(count);
            blenderDropdown.selectByIndex(index);
            selected = blenderDropdown.getFirstSelectedOption();
            cpuDropdown.selectByValue("GPU");
            gpuGroup = driver.findElement(By.id("cuda"));
            cpuGroup = driver.findElement(By.id("cpu"));
            CustomAssert.assertTrue("GPU Group Displayed", gpuGroup.isDisplayed());
            CustomAssert.assertTrue("CPU Group Not Displayed", !cpuGroup.isDisplayed());
            gpuCheckbox = driver.findElement(By.id("0"));
            CustomAssert.assertTrue("No GPU is pre-selected", !gpuCheckbox.isSelected());
            Utils.setupClickNextButton(nextButton, wait);
            WebElement alert = driver.findElement(By.xpath("//div[contains(.,'Please correct the errors')]"));
            WebElement gpuError = driver.findElement(By.xpath("//li[contains(.,'At least one GPU must be selected')]"));
            CustomAssert.assertNotNull("Alert Banner Displayed", alert);
            CustomAssert.assertNotNull("GPU Selection Error Displayed", gpuError);
        }
    }

    @After
    public void teardown() {
        driver.quit();
    }
}
