package com.dryadandnaiad.integration.setup.setupwizard;

import com.dryadandnaiad.test.utils.CustomAssert;
import com.dryadandnaiad.test.utils.Utils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created Mario Estrella on 9/4/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class ModeTest {

    private static WebDriver driver;
    private String usernameString;
    private String passwordString;
    private Integer validPort;
    private DesiredCapabilities capabilities = new DesiredCapabilities();
    private WebElement nextButton = null;
    private WebElement blenderTitle = null;
    private WebDriverWait wait;
    private WebElement computeTitle;
    private String sethlansOS = System.getProperty("os").toLowerCase();
    private String host;


    @Before
    public void setup() throws InterruptedException {
        Utils.commentGenerator("Mode Selection Test");
        capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        Utils.setDriver();
        driver = new FirefoxDriver(capabilities);
        validPort = Utils.validPortGenerator();
        usernameString = Utils.usernameGenerator(20);
        passwordString = Utils.passwordGenerator(12);
        Thread.sleep(3000);

    }

    private void settingsSetup() {
        wait = new WebDriverWait(driver, 20);
        host = "https://" + sethlansOS + ":7443";
        Utils.commentGenerator("Navigating to " + host);
        driver.get(host);
        WebElement username = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        WebElement password = driver.findElement(By.id("password"));
        try {
            Utils.slowCharEntry(username, usernameString);
            Utils.slowCharEntry(password, passwordString);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement passwordConfirm = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("passwordConfirm")));
        try {
            Utils.slowCharEntry(passwordConfirm, passwordString);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement port = driver.findElement(By.id("httpsPort"));
        port.clear();
        port.sendKeys(validPort.toString());
        Utils.setupClickNextButton(nextButton, wait);


    }

    @Test
    public void test_server_mode_selection() {
        Utils.commentGenerator("Server Mode Selection Test");
        settingsSetup();
        WebElement serverButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mode1")));
        serverButton.click();
        Utils.setupClickNextButton(nextButton, wait);
        blenderTitle = driver.findElement(By.xpath("//h3[contains(.,'Blender Download')]"));
        CustomAssert.assertNotNull("Server Config Screen Displayed", blenderTitle);

    }

    @Test
    public void test_node_mode_selection() {
        Utils.commentGenerator("Node Mode Selection Test");
        settingsSetup();
        WebElement nodeButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mode2")));
        nodeButton.click();
        Utils.setupClickNextButton(nextButton, wait);
        computeTitle = driver.findElement(By.xpath("//h3[contains(.,'Compute Method')]"));
        CustomAssert.assertNotNull("Node Config Screen Displayed", computeTitle);

    }

    @Test
    public void test_both_mode_selection() {
        Utils.commentGenerator("Dual Mode Selection Test");
        settingsSetup();
        WebElement bothButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mode3")));
        bothButton.click();
        Utils.setupClickNextButton(nextButton, wait);

        computeTitle = driver.findElement(By.xpath("//h3[contains(.,'Compute Method')]"));
        blenderTitle = driver.findElement(By.xpath("//h3[contains(.,'Blender Download')]"));

        CustomAssert.assertNotNull("Dual Config Screen Displayed", computeTitle);
        CustomAssert.assertNotNull("Dual Config Screen Displayed", blenderTitle);

    }


    @After
    public void teardown(){
        driver.quit();
    }
}
