package com.dryadandnaiad.integration.setup.setupwizard;

import com.dryadandnaiad.test.utils.CustomAssert;
import com.dryadandnaiad.test.utils.Utils;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created Mario Estrella on 9/5/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class NodeSetupTest {

    private static WebDriver driver;
    private String usernameString;
    private String passwordString;
    private Integer validPort;
    private DesiredCapabilities capabilities = new DesiredCapabilities();
    private WebElement nextButton = null;
    private WebElement port;
    private WebDriverWait wait;
    private Select cpuDropdown;
    private WebElement cores;
    private WebElement coreSlider;
    private WebElement cpuGroup;
    private WebElement gpuGroup;
    private WebElement gpuCheckbox;
    private WebElement modeSummary;
    private WebElement computeSummary;
    private WebElement coreSummary;
    private WebElement gpuSummary;
    private WebElement gpuLabel;
    private WebElement alert;
    private WebElement gpuError;
    private WebElement usernameSummary;
    private WebElement portSummary;
    private String sethlansOS = System.getProperty("os").toLowerCase();
    private String host;

    @Before
    public void setup() throws InterruptedException {
        Utils.commentGenerator("Setup Wizard Node Config Tests");
        host = "https://" + sethlansOS + ":7443";
        capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        Utils.setDriver();
        driver = new FirefoxDriver(capabilities);
        validPort = Utils.validPortGenerator();
        usernameString = Utils.usernameGenerator(20);
        passwordString = Utils.passwordGenerator(12);
        Thread.sleep(3000);
    }

    private void nodeScreen() {
        wait = new WebDriverWait(driver, 20);
        Utils.commentGenerator("Navigating to " + host);
        driver.get(host);
        WebElement username = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        WebElement password = driver.findElement(By.id("password"));
        try {
            Utils.slowCharEntry(username, usernameString);
            Utils.slowCharEntry(password, passwordString);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement passwordConfirm = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("passwordConfirm")));
        try {
            Utils.slowCharEntry(passwordConfirm, passwordString);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        port = driver.findElement(By.id("httpsPort"));
        port.clear();
        port.sendKeys(validPort.toString());
        Utils.setupClickNextButton(nextButton, wait);
        WebElement nodeButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mode2")));
        nodeButton.click();
        Utils.setupClickNextButton(nextButton, wait);

    }

    @Test
    public void test_cpu_mode() {
        Utils.commentGenerator("CPU Mode Test");
        nodeScreen();
        cpuDropdown = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("selectedMethod"))));
        cpuDropdown.selectByValue("CPU");
        coreSlider = driver.findElement(By.className("slider-handle"));
        Utils.moveSlider(driver, coreSlider);
        cores = driver.findElement(By.id("cores"));
        String value = cores.getAttribute("value");
        Utils.setupClickNextButton(nextButton, wait);
        modeSummary = driver.findElement(By.xpath("//tr[contains(.,'Mode')]"));
        computeSummary = driver.findElement(By.xpath("//tr[contains(.,'Compute Method')]"));
        coreSummary = driver.findElement(By.xpath("//tr[contains(.,'Rendering Cores')]"));
        usernameSummary = driver.findElement(By.xpath("//tr[contains(.,'User Name')]"));
        portSummary = driver.findElement(By.xpath("//tr[contains(.,'HTTPS Port')]"));
        CustomAssert.assertThat("CPU Core Summary Match", coreSummary.getAttribute("innerText"),
                CoreMatchers.containsString(value));
        CustomAssert.assertThat("CPU Compute Summary Match", computeSummary.getAttribute("innerText"),
                CoreMatchers.containsString("CPU"));
        CustomAssert.assertThat("NODE Summary Match", modeSummary.getAttribute("innerText"),
                CoreMatchers.containsString("Node"));
        CustomAssert.assertThat("Username Match", usernameSummary.getAttribute("innerText"),
                CoreMatchers.containsString(usernameString.toLowerCase()));
        CustomAssert.assertThat("Port Sumamry Match", portSummary.getAttribute("innerText"),
                CoreMatchers.containsString(validPort.toString()));
    }


    @Test
    public void test_single_gpu_mode() {
        Utils.commentGenerator("GPU Mode Test");
        nodeScreen();
        cpuDropdown = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("selectedMethod"))));
        if (cpuDropdown.getOptions().size() > 1) {
            cpuDropdown.selectByValue("GPU");
            gpuGroup = driver.findElement(By.id("cuda"));
            cpuGroup = driver.findElement(By.id("cpu"));
            CustomAssert.assertTrue("GPU Group Displayed", gpuGroup.isDisplayed());
            CustomAssert.assertTrue("CPU Group Not Displayed", !cpuGroup.isDisplayed());
            gpuCheckbox = driver.findElement(By.id("0"));
            gpuLabel = driver.findElement(By.id("0-label"));
            String gpuName = gpuLabel.getAttribute("innerText");
            CustomAssert.assertTrue("No GPU is pre-selected", !gpuCheckbox.isSelected());
            gpuCheckbox.click();
            Utils.setupClickNextButton(nextButton, wait);
            modeSummary = driver.findElement(By.xpath("//tr[contains(.,'Mode')]"));
            computeSummary = driver.findElement(By.xpath("//tr[contains(.,'Compute Method')]"));
            gpuSummary = driver.findElement(By.xpath("//tr[contains(.,'Selected GPU')]"));
            usernameSummary = driver.findElement(By.xpath("//tr[contains(.,'User Name')]"));
            portSummary = driver.findElement(By.xpath("//tr[contains(.,'HTTPS Port')]"));
            CustomAssert.assertThat("GPU Compute Match", computeSummary.getAttribute("innerText"),
                    CoreMatchers.containsString("GPU"));
            CustomAssert.assertThat("GPU Graphics Card Match", gpuSummary.getAttribute("innerText"),
                    CoreMatchers.containsString(gpuName));
            CustomAssert.assertThat("NODE Summary Match", modeSummary.getAttribute("innerText"),
                    CoreMatchers.containsString("Node"));
            CustomAssert.assertThat("Username Match", usernameSummary.getAttribute("innerText"),
                    CoreMatchers.containsString(usernameString.toLowerCase()));
            CustomAssert.assertThat("Port Sumamry Match", portSummary.getAttribute("innerText"),
                    CoreMatchers.containsString(validPort.toString()));
        }

    }

    @Test
    public void test_no_gpu_selected_gpu_mode() {
        Utils.commentGenerator("No GPU Selected in GPU Mode");
        nodeScreen();
        cpuDropdown = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("selectedMethod"))));
        if (cpuDropdown.getOptions().size() > 1) {
            cpuDropdown.selectByValue("GPU");
            gpuGroup = driver.findElement(By.id("cuda"));
            cpuGroup = driver.findElement(By.id("cpu"));
            CustomAssert.assertTrue("GPU Group Displayed", gpuGroup.isDisplayed());
            CustomAssert.assertTrue("CPU Group Not Displayed", !cpuGroup.isDisplayed());
            gpuCheckbox = driver.findElement(By.id("0"));
            CustomAssert.assertTrue("No GPU is pre-selected", !gpuCheckbox.isSelected());
            Utils.setupClickNextButton(nextButton, wait);
            alert = driver.findElement(By.xpath("//div[contains(.,'Please correct the errors')]"));
            gpuError = driver.findElement(By.xpath("//li[contains(.,'At least one GPU must be selected')]"));
            CustomAssert.assertNotNull("Alert Banner Displayed", alert);
            CustomAssert.assertNotNull("GPU Selection Error Displayed", gpuError);
        }
    }

    @Test
    public void test_cpu_gpu_mode() {
        Utils.commentGenerator("CPU and GPU Mode Test");
        nodeScreen();
        cpuDropdown = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("selectedMethod"))));
        if (cpuDropdown.getOptions().size() > 1) {
            cpuDropdown.selectByValue("CPU_GPU");
            gpuGroup = driver.findElement(By.id("cuda"));
            cpuGroup = driver.findElement(By.id("cpu"));
            CustomAssert.assertTrue("GPU Group Displayed", gpuGroup.isDisplayed());
            CustomAssert.assertTrue("CPU Group Displayed", cpuGroup.isDisplayed());
            coreSlider = driver.findElement(By.className("slider-handle"));
            Utils.moveSlider(driver, coreSlider);
            cores = driver.findElement(By.id("cores"));
            String value = cores.getAttribute("value");
            gpuCheckbox = driver.findElement(By.id("0"));
            gpuLabel = driver.findElement(By.id("0-label"));
            String gpuName = gpuLabel.getAttribute("innerText");
            CustomAssert.assertTrue("No GPU is pre-selected", !gpuCheckbox.isSelected());
            gpuCheckbox.click();
            Utils.setupClickNextButton(nextButton, wait);
            modeSummary = driver.findElement(By.xpath("//tr[contains(.,'Mode')]"));
            computeSummary = driver.findElement(By.xpath("//tr[contains(.,'Compute Method')]"));
            coreSummary = driver.findElement(By.xpath("//tr[contains(.,'Rendering Cores')]"));
            gpuSummary = driver.findElement(By.xpath("//tr[contains(.,'Selected GPU')]"));
            usernameSummary = driver.findElement(By.xpath("//tr[contains(.,'User Name')]"));
            portSummary = driver.findElement(By.xpath("//tr[contains(.,'HTTPS Port')]"));
            CustomAssert.assertThat("GPU Graphics Card Match", gpuSummary.getAttribute("innerText"),
                    CoreMatchers.containsString(gpuName));
            CustomAssert.assertThat("NODE Summary Match", modeSummary.getAttribute("innerText"),
                    CoreMatchers.containsString("Node"));
            CustomAssert.assertThat("Username Match", usernameSummary.getAttribute("innerText"),
                    CoreMatchers.containsString(usernameString.toLowerCase()));
            CustomAssert.assertThat("Port Sumamry Match", portSummary.getAttribute("innerText"),
                    CoreMatchers.containsString(validPort.toString()));
            CustomAssert.assertThat("CPU_GPU Compute Match", computeSummary.getAttribute("innerText"),
                    CoreMatchers.containsString("CPU & GPU"));
            CustomAssert.assertThat("CPU Core Summary Match", coreSummary.getAttribute("innerText"),
                    CoreMatchers.containsString(value));
        }
    }

    @Test
    public void test_no_gpu_selected_cpugpu_mode() {
        Utils.commentGenerator("No GPU Selected in CPU & GPU Mode");
        nodeScreen();
        cpuDropdown = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("selectedMethod"))));
        if (cpuDropdown.getOptions().size() > 1) {
            cpuDropdown.selectByValue("CPU_GPU");
            gpuGroup = driver.findElement(By.id("cuda"));
            cpuGroup = driver.findElement(By.id("cpu"));
            CustomAssert.assertTrue("GPU Group Displayed", gpuGroup.isDisplayed());
            CustomAssert.assertTrue("CPU Group Displayed", cpuGroup.isDisplayed());
            coreSlider = driver.findElement(By.className("slider-handle"));
            Utils.moveSlider(driver, coreSlider);
            gpuCheckbox = driver.findElement(By.id("0"));
            CustomAssert.assertTrue("No GPU is pre-selected", !gpuCheckbox.isSelected());
            Utils.setupClickNextButton(nextButton, wait);
            alert = driver.findElement(By.xpath("//div[contains(.,'Please correct the errors')]"));
            gpuError = driver.findElement(By.xpath("//li[contains(.,'At least one GPU must be selected')]"));
            CustomAssert.assertNotNull("Alert Banner Displayed", alert);
            CustomAssert.assertNotNull("GPU Selection Error Displayed", gpuError);
        }
    }

    @After
    public void teardown() {
        driver.quit();
    }

}
