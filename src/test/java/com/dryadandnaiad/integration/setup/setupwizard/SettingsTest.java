package com.dryadandnaiad.integration.setup.setupwizard;

import com.dryadandnaiad.test.utils.CustomAssert;
import com.dryadandnaiad.test.utils.Utils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


/**
 * Created Mario Estrella on 9/3/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlansintegration
 */
public class SettingsTest {
    private static WebDriver driver;
    private String usernameString;
    private String passwordString;
    private String passwordStringConfirmIncorrect;
    private String passwordTooShort;
    private String usernameTooShort;
    private Integer validPort;
    private String host;
    private Integer invalidPort;
    private DesiredCapabilities capabilities = new DesiredCapabilities();
    private WebElement username;
    private WebElement password;
    private WebElement passwordConfirm;
    private WebElement modeTitle = null;
    private WebElement nextButton = null;
    private WebElement port;
    private WebDriverWait wait;
    private WebElement alert = null;
    private WebElement passwordError = null;
    private String sethlansOS = System.getProperty("os").toLowerCase();


    @Before
    public void setup() throws InterruptedException {
        host = "https://" + sethlansOS + ":7443";
        capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        Utils.setDriver();
        driver = new FirefoxDriver(capabilities);
        validPort = Utils.validPortGenerator();
        usernameString = Utils.usernameGenerator(20);
        passwordString = Utils.passwordGenerator(12);
        invalidPort = 65582;
        passwordStringConfirmIncorrect = Utils.passwordGenerator(13);
        passwordTooShort = Utils.passwordGenerator(5);
        usernameTooShort = Utils.usernameGenerator(3);
        Thread.sleep(3000);

    }

    @Test
    public void test_password_valid() throws InterruptedException {
        Utils.commentGenerator("Valid Password Test");
        wait = new WebDriverWait(driver, 20);
        Utils.commentGenerator("Navigating to " + host);
        driver.get(host);
        username = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        password = driver.findElement(By.id("password"));
        Utils.slowCharEntry(username, usernameString);
        Utils.slowCharEntry(password, passwordString);
        passwordConfirm = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("passwordConfirm")));
        Utils.slowCharEntry(passwordConfirm, passwordString);
        Utils.setupClickNextButton(nextButton, wait);
        modeTitle = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(.,'Mode')]")));
        CustomAssert.assertNotNull("Mode selection page displayed", modeTitle);

    }

    @Test
    public void test_password_confirm_invalid() throws InterruptedException {
        Utils.commentGenerator("Invalid Password Confirmation Test");
        wait = new WebDriverWait(driver, 20);
        Utils.commentGenerator("Navigating to " + host);
        driver.get(host);
        username = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        password = driver.findElement(By.id("password"));
        Utils.slowCharEntry(username, usernameString);
        Utils.slowCharEntry(password, passwordString);
        passwordConfirm = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("passwordConfirm")));
        Utils.slowCharEntry(passwordConfirm, passwordStringConfirmIncorrect);
        Utils.setupClickNextButton(nextButton, wait);
        alert = driver.findElement(By.xpath("//div[contains(.,'Please correct the errors')]"));
        passwordError = driver.findElement(By.xpath("//li[contains(.,'Password values')]"));
        CustomAssert.assertNotNull("Alert Banner Displayed", alert);
        CustomAssert.assertNotNull("Password Confirm Error Displayed", passwordError);
    }

    @Test
    public void test_password_too_short() throws InterruptedException {
        Utils.commentGenerator("Password Too Short Test");
        wait = new WebDriverWait(driver, 20);
        Utils.commentGenerator("Navigating to " + host);
        driver.get(host);
        username = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        password = driver.findElement(By.id("password"));
        Utils.slowCharEntry(username, usernameString);
        Utils.slowCharEntry(password, passwordTooShort);
        Utils.setupClickNextButton(nextButton, wait);
        alert = driver.findElement(By.xpath("//div[contains(.,'Please correct the errors')]"));
        passwordError = driver.findElement(By.xpath("//li[contains(.,'Password needs to be a minimum')]"));
        CustomAssert.assertNotNull("Alert Banner Displayed", alert);
        CustomAssert.assertNotNull("Password Size Error Displayed", passwordError);
    }

    @Test
    public void test_password_invalid_chars() throws InterruptedException {
        Utils.commentGenerator("Invalid Characters in Password Test");
        String invalidPassword = Utils.invalidPasswordGenerator(15);
        wait = new WebDriverWait(driver, 20);
        Utils.commentGenerator("Navigating to " + host);
        driver.get(host);
        username = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        password = driver.findElement(By.id("password"));
        Utils.slowCharEntry(username, usernameString);
        Utils.slowCharEntry(password, invalidPassword);
        Utils.setupClickNextButton(nextButton, wait);
        alert = driver.findElement(By.xpath("//div[contains(.,'Please correct the errors')]"));
        passwordError = driver.findElement(By.xpath("//li[contains(.,'The following symbols are not supported')]"));
        CustomAssert.assertNotNull("Alert Banner Displayed", alert);
        CustomAssert.assertNotNull("Password Invalid Characters Error Displayed", passwordError);
    }

    @Test
    public void test_username_too_short() throws InterruptedException {
        Utils.commentGenerator("Username too short Test");
        wait = new WebDriverWait(driver, 20);
        Utils.commentGenerator("Navigating to " + host);
        driver.get(host);
        username = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        Utils.slowCharEntry(username, usernameTooShort);
        Utils.setupClickNextButton(nextButton, wait);
        alert = driver.findElement(By.xpath("//div[contains(.,'Please correct the errors')]"));
        WebElement usernameError = driver.findElement(By.xpath("//li[contains(.,'size must be between 4')]"));
        CustomAssert.assertNotNull("Alert Banner Displayed", alert);
        CustomAssert.assertNotNull("Username Size Error Displayed", usernameError);
    }

    @Test
    public void test_username_invalid_chars() throws InterruptedException {
        Utils.commentGenerator("Invalid Characters in Username Test");
        String invalidUsername = Utils.invalidUsernameGenerator(20);
        wait = new WebDriverWait(driver, 20);
        Utils.commentGenerator("Navigating to " + host);
        driver.get(host);
        username = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        Utils.slowCharEntry(username, invalidUsername);
        Utils.setupClickNextButton(nextButton, wait);
        alert = driver.findElement(By.xpath("//div[contains(.,'Please correct the errors')]"));
        WebElement usernameError = driver.findElement(By.xpath("//li[contains(.,'User names can only contain alpha-numeric characters')]"));
        CustomAssert.assertNotNull("Alert Banner Displayed", alert);
        CustomAssert.assertNotNull("Username Invalid Chars Error Displayed", usernameError);
    }

    @Test
    public void test_valid_port() throws InterruptedException {
        Utils.commentGenerator("Valid Port Value Entered Test");
        wait = new WebDriverWait(driver, 20);
        Utils.commentGenerator("Navigating to " + host);
        driver.get(host);
        username = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        password = driver.findElement(By.id("password"));
        Utils.slowCharEntry(username, usernameString);
        Utils.slowCharEntry(password, passwordString);
        passwordConfirm = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("passwordConfirm")));
        Utils.slowCharEntry(passwordConfirm, passwordString);
        port = driver.findElement(By.id("httpsPort"));
        port.clear();
        port.sendKeys(validPort.toString());
        Utils.setupClickNextButton(nextButton, wait);
        modeTitle = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(.,'Mode')]")));
        CustomAssert.assertNotNull("Mode selection page displayed", modeTitle);
    }

    @Test
    public void test_invalid_port() throws InterruptedException {
        Utils.commentGenerator("Invalid Port Value Entered Test");
        wait = new WebDriverWait(driver, 20);
        Utils.commentGenerator("Navigating to " + host);
        driver.get(host);
        username = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        password = driver.findElement(By.id("password"));
        Utils.slowCharEntry(username, usernameString);
        Utils.slowCharEntry(password, passwordTooShort);
        passwordConfirm = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("passwordConfirm")));
        Utils.slowCharEntry(passwordConfirm, passwordString);
        port = driver.findElement(By.id("httpsPort"));
        port.clear();
        port.sendKeys(invalidPort.toString());
        Utils.setupClickNextButton(nextButton, wait);
        alert = driver.findElement(By.xpath("//div[contains(.,'Please correct the errors')]"));
        WebElement portError = driver.findElement(By.xpath("//li[contains(.,'must be less than or equal to 65535')]"));
        CustomAssert.assertNotNull("Alert Banner Displayed", alert);
        CustomAssert.assertNotNull("Invalid Port Error Displayed", portError);
    }

    @After
    public void teardown(){
        driver.quit();
    }


}
