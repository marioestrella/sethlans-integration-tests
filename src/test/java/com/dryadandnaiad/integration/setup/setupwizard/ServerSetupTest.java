package com.dryadandnaiad.integration.setup.setupwizard;

import com.dryadandnaiad.test.utils.CustomAssert;
import com.dryadandnaiad.test.utils.Utils;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

/**
 * Created Mario Estrella on 9/5/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class ServerSetupTest {

    private static WebDriver driver;
    private String usernameString;
    private String passwordString;
    private Integer validPort;
    private DesiredCapabilities capabilities = new DesiredCapabilities();
    private WebElement nextButton = null;
    private WebDriverWait wait;
    private Select blenderDropdown;
    private WebElement selected;
    private WebElement versionSummary;
    private WebElement modeSummary;
    private WebElement usernameSummary;
    private WebElement portSummary;
    private String sethlansOS = System.getProperty("os").toLowerCase();
    private String host;

    @Before
    public void setup() throws InterruptedException {
        Utils.commentGenerator("Setup Wizard Server Config Tests");
        capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        Utils.setDriver();
        driver = new FirefoxDriver(capabilities);
        validPort = Utils.validPortGenerator();
        usernameString = Utils.usernameGenerator(20);
        passwordString = Utils.passwordGenerator(12);
        Thread.sleep(3000);
    }

    private void serverScreen() {
        host = "https://" + sethlansOS + ":7443";
        wait = new WebDriverWait(driver, 20);
        Utils.commentGenerator("Navigating to " + host);
        driver.get(host);
        WebElement username = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        WebElement password = driver.findElement(By.id("password"));
        try {
            Utils.slowCharEntry(username, usernameString);
            Utils.slowCharEntry(password, passwordString);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement passwordConfirm = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("passwordConfirm")));
        try {
            Utils.slowCharEntry(passwordConfirm, passwordString);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement port = driver.findElement(By.id("httpsPort"));
        port.clear();
        port.sendKeys(validPort.toString());
        Utils.setupClickNextButton(nextButton, wait);
        WebElement serverButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mode1")));
        serverButton.click();
        Utils.setupClickNextButton(nextButton, wait);

    }

    @Test
    public void test_summary_page() {
        Utils.commentGenerator("Summary Page Test");
        serverScreen();
        blenderDropdown = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("blenderVersion"))));
        selected = blenderDropdown.getAllSelectedOptions().get(0);
        String blenderVersion = selected.getAttribute("innerText");
        Utils.setupClickNextButton(nextButton, wait);
        versionSummary = driver.findElement(By.xpath("//tr[contains(.,'Blender Version')]"));
        modeSummary = driver.findElement(By.xpath("//tr[contains(.,'Mode')]"));
        usernameSummary = driver.findElement(By.xpath("//tr[contains(.,'User Name')]"));
        portSummary = driver.findElement(By.xpath("//tr[contains(.,'HTTPS Port')]"));
        Utils.commentGenerator("Sumamry Assertions");
        CustomAssert.assertThat("SERVER Summary Match", modeSummary.getAttribute("innerText"),
                CoreMatchers.containsString("Server"));
        CustomAssert.assertThat("Blender Version Summary Match", versionSummary.getAttribute("innerText"),
                CoreMatchers.containsString(blenderVersion));
        CustomAssert.assertThat("Username Match", usernameSummary.getAttribute("innerText"),
                CoreMatchers.containsString(usernameString.toLowerCase()));
        CustomAssert.assertThat("Port Sumamry Match", portSummary.getAttribute("innerText"),
                CoreMatchers.containsString(validPort.toString()));
    }

    @Test
    public void test_verify_dropdown_list() {
        Utils.commentGenerator("Blender Dropdown Test");
        serverScreen();
        blenderDropdown = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("blenderVersion"))));
        List<WebElement> dropdownList = blenderDropdown.getOptions();
        CustomAssert.assertTrue("Dropdown List is greater than 1", dropdownList.size() > 1);
        CustomAssert.assertNotNull("Dropdown List Not Null", dropdownList.get(1).getAttribute("innerText"));
    }

    @Test
    public void test_blender_dropdown_summary() {
        Utils.commentGenerator("Blender Version Summary Test");
        Random r = new Random();
        serverScreen();
        blenderDropdown = new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("blenderVersion"))));
        int count = blenderDropdown.getOptions().size();
        int value = r.nextInt(count);
        blenderDropdown.selectByIndex(value);
        selected = blenderDropdown.getFirstSelectedOption();
        String blenderVersion = selected.getAttribute("innerText");
        Utils.setupClickNextButton(nextButton, wait);
        versionSummary = driver.findElement(By.xpath("//tr[contains(.,'Blender Version')]"));
        modeSummary = driver.findElement(By.xpath("//tr[contains(.,'Mode')]"));
        usernameSummary = driver.findElement(By.xpath("//tr[contains(.,'User Name')]"));
        portSummary = driver.findElement(By.xpath("//tr[contains(.,'HTTPS Port')]"));
        CustomAssert.assertThat("SERVER Summary Match", modeSummary.getAttribute("innerText"),
                CoreMatchers.containsString("Server"));
        CustomAssert.assertThat("Blender Version Summary Match", versionSummary.getAttribute("innerText"),
                CoreMatchers.containsString(blenderVersion));
        CustomAssert.assertThat("Username Match", usernameSummary.getAttribute("innerText"),
                CoreMatchers.containsString(usernameString.toLowerCase()));
        CustomAssert.assertThat("Port Sumamry Match", portSummary.getAttribute("innerText"),
                CoreMatchers.containsString(validPort.toString()));
    }

    @After
    public void teardown(){
        driver.quit();
    }
}
