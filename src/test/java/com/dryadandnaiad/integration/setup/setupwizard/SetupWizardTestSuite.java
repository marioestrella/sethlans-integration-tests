package com.dryadandnaiad.integration.setup.setupwizard;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created Mario Estrella on 10/12/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
        SettingsTest.class,
        ModeTest.class,
        ServerSetupTest.class,
        NodeSetupTest.class,
        DualSetupTest.class
})
public class SetupWizardTestSuite {
}
