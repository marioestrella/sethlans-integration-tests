package com.dryadandnaiad.integration.setup.node;

import com.dryadandnaiad.integration.setup.AbstractSetupWizardComplete;
import com.dryadandnaiad.test.utils.ComputeOptions;
import com.dryadandnaiad.test.utils.NodeSetup;
import org.junit.BeforeClass;
import org.junit.Test;

public class SetupWizardNodeCompleteCPUGPU extends AbstractSetupWizardComplete {

    @BeforeClass
    public static void nodeSetup() throws InterruptedException {
        NodeSetup nodeSetup = new NodeSetup();
        nodeSetup.startSetup(wait, driver, setupHost, sethlansOS, usernameString, passwordString, validPort, ComputeOptions.CPU_GPU);
    }

    @Test
    public void test_node_cpu_gpu_setup_complete() throws InterruptedException {
        setupCompleteTest("Node");
    }



}
