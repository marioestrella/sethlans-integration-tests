package com.dryadandnaiad.integration.setup.node;

import com.dryadandnaiad.integration.setup.AbstractSetupWizardComplete;
import com.dryadandnaiad.test.utils.ComputeOptions;
import com.dryadandnaiad.test.utils.NodeSetup;
import org.junit.BeforeClass;
import org.junit.Test;

public class SetupWizardNodeCompleteGPU extends AbstractSetupWizardComplete {

    @BeforeClass
    public static void nodeSetup() throws InterruptedException {
        NodeSetup nodeSetup = new NodeSetup();
        nodeSetup.startSetup(wait, driver, setupHost, sethlansOS, usernameString, passwordString, validPort, ComputeOptions.GPU);
    }

    @Test
    public void test_node_gpu_setup_complete() throws InterruptedException {
        setupCompleteTest("Node");
    }



}
