package com.dryadandnaiad.integration.setup.dual;

import com.dryadandnaiad.integration.setup.AbstractSetupWizardComplete;
import com.dryadandnaiad.test.utils.ComputeOptions;
import com.dryadandnaiad.test.utils.DualSetup;
import org.junit.BeforeClass;
import org.junit.Test;

public class SetupWizardDualCompleteCPU extends AbstractSetupWizardComplete {


    @BeforeClass
    public static void dualSetup() throws InterruptedException {
        DualSetup dualSetup = new DualSetup();
        dualSetup.startSetup(wait, driver, setupHost, sethlansOS, usernameString, passwordString, validPort, ComputeOptions.CPU);
    }


    @Test
    public void test_dual_cpu_setup_complete() throws InterruptedException {
        setupCompleteTest("Dual");
    }




}
