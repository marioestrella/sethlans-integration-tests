package com.dryadandnaiad.integration.setup.dual;

import com.dryadandnaiad.integration.setup.AbstractSetupWizardComplete;
import com.dryadandnaiad.test.utils.ComputeOptions;
import com.dryadandnaiad.test.utils.DualSetup;
import org.junit.BeforeClass;
import org.junit.Test;

public class SetupWizardDualCompleteCPUGPU extends AbstractSetupWizardComplete {

    @BeforeClass
    public static void dualSetup() throws InterruptedException {
        DualSetup dualSetup = new DualSetup();
        dualSetup.startSetup(wait, driver, setupHost, sethlansOS, usernameString, passwordString, validPort, ComputeOptions.CPU_GPU);
    }


    @Test
    public void test_dual_cpu_gpu_setup_complete() throws InterruptedException {
        setupCompleteTest("Dual");
    }




}
