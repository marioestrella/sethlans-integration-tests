package com.dryadandnaiad.integration.security;

import com.dryadandnaiad.test.utils.*;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created Mario Estrella on 9/24/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class LoginTestsDual {

    private static WebDriver driver;
    private static String usernameString;
    private static String passwordString;
    private static Integer validPort;
    private static String sethlansOS = System.getProperty("os").toLowerCase();
    private static DesiredCapabilities capabilities = new DesiredCapabilities();
    private static WebDriverWait wait;
    private static String setupHost;
    private String host;

    @BeforeClass
    public static void setup() throws InterruptedException {
        Utils.commentGenerator("Executing Login Tests - Dual");
        setupHost = "https://" + sethlansOS + ":7443" + "/";
        Utils.setDriver();
        capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        driver = new FirefoxDriver(capabilities);
        wait = new WebDriverWait(driver, 60);
        validPort = Utils.validPortGenerator(7000, 8000);
        usernameString = Utils.usernameGenerator(6);
        passwordString = Utils.passwordGenerator(10) + "VxZy";
        Utils.commentGenerator("Username used during test: " + usernameString);
        Utils.commentGenerator("Password used during test: " + passwordString);
        Utils.commentGenerator("Sethlans Port: " + validPort);
        DualSetup dualSetup = new DualSetup();
        dualSetup.startSetup(wait, driver, setupHost, sethlansOS, usernameString, passwordString, validPort, ComputeOptions.CPU);
    }

    @Before
    public void setHost() throws InterruptedException {
        host = "https://" + sethlansOS + ":" + validPort.toString() + "/";
        Utils.setDriver();
        capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        driver = new FirefoxDriver(capabilities);
        wait = new WebDriverWait(driver, 60);
        Thread.sleep(5000);
    }

    @Test
    public void test_login_success() throws InterruptedException {
        Utils.commentGenerator("Testing Login Success");
        Utils.commentGenerator("Navigating to " + host + "home");
        driver.get(host + "home");
        Login login = new Login();
        login.sethlansLogin(usernameString, passwordString, wait, driver);
        String currentURL = driver.getCurrentUrl();
        CustomAssert.assertEquals("Login Success Test Complete", host + "home", currentURL);

    }

    @Test
    public void test_login_failure_password() throws InterruptedException {
        Utils.commentGenerator("Testing Login Failure using Invalid Passwords");
        Utils.commentGenerator("Navigating to " + host + "home");
        String invalidPassword = Utils.passwordGenerator(8);
        driver.get(host + "home");
        Login login = new Login();
        login.sethlansLogin(usernameString, invalidPassword, wait, driver);
        String currentURL = driver.getCurrentUrl();
        CustomAssert.assertEquals("Login Failure Test Complete", host + "login?error", currentURL);
    }

    @Test
    public void test_login_failure_username() throws InterruptedException {
        Utils.commentGenerator("Testing Login Failure using Invalid Usernames");
        Utils.commentGenerator("Navigating to " + host + "home");
        String invalidUsername = Utils.usernameGenerator(8);
        driver.get(host + "home");
        Login login = new Login();
        login.sethlansLogin(invalidUsername, passwordString, wait, driver);
        String currentURL = driver.getCurrentUrl();
        CustomAssert.assertEquals("Login Failure Username Test Complete", host + "login?error", currentURL);
    }

    @Test
    public void test_login_success_username_caseinsensitive() throws InterruptedException {
        Utils.commentGenerator("Testing Login Success using Usernames in uppercase and lowercase");
        Utils.commentGenerator("Navigating to " + host + "home");
        driver.get(host + "home");
        Login login = new Login();
        login.sethlansLogin(usernameString.toUpperCase(), passwordString, wait, driver);
        String currentUrl = driver.getCurrentUrl();
        CustomAssert.assertEquals("Login Test with Uppercase", host + "home", currentUrl);
        Assert.assertEquals(host + "home", currentUrl);
        driver.get(host + "logout");
        login.sethlansLogin(usernameString.toLowerCase(), passwordString, wait, driver);
        currentUrl = driver.getCurrentUrl();
        CustomAssert.assertEquals("Login Test with Lowercase", host + "home", currentUrl);

    }

    @Test
    public void test_login_failure_password_case_insensitive() throws InterruptedException {
        Utils.commentGenerator("Test Login Failure Password case sensitivity");
        Utils.commentGenerator("Navigating to " + host + "home");
        driver.get(host + "home");
        Login login = new Login();
        login.sethlansLogin(usernameString, passwordString.toUpperCase(), wait, driver);
        String currentURL = driver.getCurrentUrl();
        CustomAssert.assertEquals("Login Failure Invalid Password - Uppercase", host + "login?error", currentURL);
        driver.get(host + "home");
        login.sethlansLogin(usernameString, passwordString.toLowerCase(), wait, driver);
        currentURL = driver.getCurrentUrl();
        CustomAssert.assertEquals("Login Failure Invalid Password - Lowercase", host + "login?error", currentURL);

    }

    @Test
    public void test_dropdown_logout() throws InterruptedException {
        Utils.commentGenerator("Testing Logout via Menu Option");
        Utils.commentGenerator("Navigating to " + host + "home");
        driver.get(host + "home");
        Login login = new Login();
        login.sethlansLogin(usernameString, passwordString, wait, driver);
        String currentUrl = driver.getCurrentUrl();
        Assert.assertEquals(host + "home", currentUrl);
        WebElement userMenu = driver.findElement(By.id("userMenu"));
        userMenu.click();
        WebElement dropdownLogout = driver.findElement(By.id("dropdown-logout"));
        dropdownLogout.click();
        currentUrl = driver.getCurrentUrl();
        CustomAssert.assertEquals("Logout via Dropdown Test Complete", host + "login?logout", currentUrl);
    }


    @Test
    public void test_username_nav() throws InterruptedException {
        Utils.commentGenerator("Verify Menu Contains valid username");
        Utils.commentGenerator("Navigating to " + host + "home");
        driver.get(host + "home");
        Login login = new Login();
        login.sethlansLogin(usernameString, passwordString, wait, driver);
        WebElement userID = driver.findElement(By.id("currentLoggedInUser"));
        CustomAssert.assertEquals("Username in Navbar Test Complete", usernameString.toLowerCase(), userID.getAttribute("innerText"));
    }

    @After
    public void logout() {
        Utils.commentGenerator("Current test completed, logging out and quitting webdriver ");
        driver.get(host + "logout");
        driver.quit();

    }

    @AfterClass
    public static void teardown() throws InterruptedException {
        Utils.commentGenerator("Login Tests Dual Completed");
        Utils.setDriver();
        capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        driver = new FirefoxDriver(capabilities);
        setupHost = "https://" + sethlansOS + ":" + validPort + "/shutdown";
        wait = new WebDriverWait(driver, 60);
        Utils.commentGenerator("Shutting Down Sethlans");
        Utils.cleanup(setupHost, driver, wait, usernameString, passwordString);
    }
}
