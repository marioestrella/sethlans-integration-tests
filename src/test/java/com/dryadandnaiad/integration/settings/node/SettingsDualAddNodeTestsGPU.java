package com.dryadandnaiad.integration.settings.node;

import com.dryadandnaiad.test.utils.ComputeOptions;
import com.dryadandnaiad.test.utils.DualAddNode;
import com.dryadandnaiad.test.utils.DualSetup;
import com.dryadandnaiad.test.utils.Utils;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created Mario Estrella on 11/10/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class SettingsDualAddNodeTestsGPU extends AbstractDualAddNode {

    @BeforeClass
    public static void dualSetup() throws InterruptedException {
        DualSetup dualSetup = new DualSetup();
        dualSetup.startSetup(wait, driver, setupHost, sethlansOS, usernameString, passwordString, validPort, ComputeOptions.GPU);
    }

    @Test
    public void test_add_gpu_node() throws InterruptedException {
        Utils.commentGenerator("Starting Settings - Add GPU Node");
        DualAddNode cpuDualAddNode = new DualAddNode();
        cpuDualAddNode.addNode(driver, sethlansOS, validPort, host, ComputeOptions.GPU, true);
    }

    @Test
    public void test_discovery_gpu_node() throws InterruptedException {
        Utils.commentGenerator("Starting Settings - Discover GPU Node");
        DualAddNode cpuDualAddNode = new DualAddNode();
        cpuDualAddNode.discoverNode(driver, sethlansOS, validPort, host, ComputeOptions.GPU, true);

    }



}
