package com.dryadandnaiad.integration.dual_benchmark;

import com.dryadandnaiad.test.utils.*;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created Mario Estrella on 12/19/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class BenchmarkTestCPUGPU extends AbstractBenchmarkTest {

    @BeforeClass
    public static void dualSetup() throws InterruptedException {
        Utils.commentGenerator("Starting Benchmark Test - CPU and GPU Node");
        DualSetup dualSetup = new DualSetup();
        dualSetup.startSetup(wait, driver, setupHost, sethlansOS, usernameString, passwordString, validPort, ComputeOptions.CPU_GPU);
    }

    @Test
    public void test_add_cpu_gpu_node() throws InterruptedException {
        WebElement computeSettings = driver.findElement(By.id("settings_compute"));
        computeSettings.click();
        Thread.sleep(1000);
        WebElement cpuGroup = driver.findElement(By.id("cpu"));
        Utils.selectMaxSliderValue(driver, cpuGroup);
        WebElement updateButton = driver.findElement(By.id("update_method"));
        updateButton.click();
        Thread.sleep(20000);
        Login login = new Login();
        login.sethlansLogin(usernameString, passwordString, wait, driver);
        Thread.sleep(2000);
        WebElement menuButton = driver.findElement(By.id("settings"));
        menuButton.click();
        Utils.commentGenerator("Loading Settings");
        Thread.sleep(2000);
        DualBenchmarkNode cpuGPUDualBenchmarkNode = new DualBenchmarkNode();
        cpuGPUDualBenchmarkNode.startBenchmark(driver, sethlansOS, validPort, host, ComputeOptions.CPU_GPU, wait);
    }
}
