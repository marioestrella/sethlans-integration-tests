package com.dryadandnaiad.integration.dual_benchmark;

import com.dryadandnaiad.test.utils.ComputeOptions;
import com.dryadandnaiad.test.utils.DualBenchmarkNode;
import com.dryadandnaiad.test.utils.DualSetup;
import com.dryadandnaiad.test.utils.Utils;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created Mario Estrella on 12/19/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class BenchmarkTestGPU extends AbstractBenchmarkTest {

    @BeforeClass
    public static void dualSetup() throws InterruptedException {
        Utils.commentGenerator("Starting Benchmark Test - GPU Node");
        DualSetup dualSetup = new DualSetup();
        dualSetup.startSetup(wait, driver, setupHost, sethlansOS, usernameString, passwordString, validPort, ComputeOptions.GPU);
    }

    @Test
    public void test_add_gpu_node() throws InterruptedException {
        DualBenchmarkNode gpuDualBenchmarkNode = new DualBenchmarkNode();
        gpuDualBenchmarkNode.startBenchmark(driver, sethlansOS, validPort, host, ComputeOptions.GPU, wait);
    }
}
