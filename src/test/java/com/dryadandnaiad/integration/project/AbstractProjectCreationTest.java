package com.dryadandnaiad.integration.project;

import com.dryadandnaiad.test.utils.Login;
import com.dryadandnaiad.test.utils.Utils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created Mario Estrella on 12/22/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class AbstractProjectCreationTest {

    static WebDriver driver;
    static String usernameString;
    static String passwordString;
    static Integer validPort;
    static String sethlansOS = System.getProperty("os").toLowerCase();
    static DesiredCapabilities capabilities = new DesiredCapabilities();
    static WebDriverWait wait;
    static String setupHost;
    String host;

    @BeforeClass
    public static void setup() {
        Utils.commentGenerator("Executing Project Creation/Render Tests");
        setupHost = "https://" + sethlansOS + ":7443" + "/";
        Utils.setDriver();
        capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        driver = new FirefoxDriver(capabilities);
        wait = new WebDriverWait(driver, 60);
        validPort = Utils.validPortGenerator(7000, 8000);
        usernameString = Utils.usernameGenerator(6);
        passwordString = Utils.passwordGenerator(10) + "VxZy";
        Utils.commentGenerator("Username used during test: " + usernameString);
        Utils.commentGenerator("Password used during test: " + passwordString);
        Utils.commentGenerator("Sethlans Port: " + validPort);
    }

    @Before
    public void setHost() throws InterruptedException {
        host = "https://" + sethlansOS + ":" + validPort.toString() + "/";
        Utils.setDriver();
        capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        driver = new FirefoxDriver(capabilities);
        wait = new WebDriverWait(driver, 60);
        Thread.sleep(5000);
        Utils.commentGenerator("Navigating to " + host + "home");
        driver.get(host + "home");
        Login login = new Login();
        login.sethlansLogin(usernameString, passwordString, wait, driver);
        Thread.sleep(2000);
        WebElement dropdown = driver.findElement(By.id("notifications"));
        dropdown.click();
        Thread.sleep(1000);
        Utils.commentGenerator("Checking Blender Binary Download State");
        WebElement message = driver.findElement(By.id("message"));
        while (message != null) {
            dropdown = driver.findElement(By.id("notifications"));
            dropdown.click();
            Thread.sleep(1000);
            message = driver.findElement(By.id("message"));
            if (!message.getAttribute("innerText").contains("Downloading Blender")) {
                message = null;
                Utils.commentGenerator("Blender Download is Complete!");
                driver.navigate().refresh();
            } else {
                Utils.commentGenerator("Waiting for Blender Binary to Finish Downloading.");
            }
            Thread.sleep(10000);
            driver.navigate().refresh();
        }
    }

    @After
    public void logout() {
        Utils.commentGenerator("Current test completed, logging out and quitting webdriver");
        driver.get(host + "logout");
        driver.quit();
    }

    @AfterClass
    public static void teardown() {
        Utils.commentGenerator("Tests Completed");
        Utils.setDriver();
        capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        driver = new FirefoxDriver(capabilities);
        setupHost = "https://" + sethlansOS + ":" + validPort + "/shutdown";
        wait = new WebDriverWait(driver, 60);
        Utils.commentGenerator("Shutting Down Sethlans");
        Utils.cleanup(setupHost, driver, wait, usernameString, passwordString);
    }
}
