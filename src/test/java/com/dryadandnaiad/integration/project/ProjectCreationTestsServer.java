package com.dryadandnaiad.integration.project;

import com.dryadandnaiad.test.utils.*;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created Mario Estrella on 10/18/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class ProjectCreationTestsServer extends AbstractProjectCreationTest {

    @BeforeClass
    public static void serverSetup() throws InterruptedException {
        ServerSetup serverSetup = new ServerSetup();
        serverSetup.startSetup(wait, driver, setupHost, sethlansOS, usernameString, passwordString, validPort);

    }


    @Test
    public void test_empty_project_name() throws InterruptedException {
        Projects projects = new Projects();
        projects.emptyProjectName(driver, host);
    }

    @Test
    public void test_short_project_name() throws InterruptedException {
        Projects newProject = new Projects();
        newProject.shortProjectName(driver, host);
    }

    @Test
    public void test_create_bmw_cpu_still_project() throws InterruptedException {
        Utils.commentGenerator("Starting BMW CPU - Still Project");
        Projects newProject = new Projects();
        newProject.createStillImageProject(driver, host, wait, "bmw27_cpu.blend",
                ComputeOptions.CPU, EngineOptions.CYCLES, true, null, null,
                true, null, false, true);

    }

    @Test
    public void test_create_bmw_gpu_still_project() throws InterruptedException {
        Utils.commentGenerator("Starting BMW GPU - Still Project");
        Projects newProject = new Projects();
        newProject.createStillImageProject(driver, host, wait, "bmw27_gpu.blend",
                ComputeOptions.GPU, EngineOptions.CYCLES, false, "3840", "2160", true,
                "0", false, true);
    }

    @Test
    public void test_create_teeglasfx_still_project() throws InterruptedException {
        Utils.commentGenerator("Starting TeeglasFX - Still Project");
        Projects newProject = new Projects();
        newProject.createStillImageProject(driver, host, wait, "TeeglasFX_27.blend",
                ComputeOptions.CPU, EngineOptions.CYCLES, true, null, null,
                true, null, false, true);
    }

    @Test
    public void test_create_helicopter_still_project() throws InterruptedException {
        Utils.commentGenerator("Starting Helicopter Scene - Still Project");
        Projects newProject = new Projects();
        newProject.createStillImageProject(driver, host, wait, "scene-Helicopter-27.blend",
                ComputeOptions.CPU_GPU, EngineOptions.CYCLES, false, "2580", "1080",
                false, "500", false, true);
    }

    @Test
    public void test_create_volume_emission_still_project() throws InterruptedException {
        Utils.commentGenerator("Starting Volume Emission - Still Image Project - Blender Render");
        Projects newProject = new Projects();
        newProject.createStillImageProject(driver, host, wait, "volume_emission_27.blend",
                ComputeOptions.CPU, EngineOptions.BLENDER_RENDER, false, "900", "900",
                true, null, false, true);
    }

    @Test
    public void test_create_bmw_cpu_animation_project() throws InterruptedException {
        Utils.commentGenerator("Starting BMW CPU - Animation Project");
        Projects newProject = new Projects();
        newProject.createAnimationProject(driver, host, wait, "bmw27_cpu.blend",
                ComputeOptions.CPU, EngineOptions.CYCLES, true, null, null,
                true, null, true, null, null,
                null, true, null,
                false, true);

    }

    @Test
    public void test_create_bmw_gpu_animation_project() throws InterruptedException {
        Utils.commentGenerator("Starting BMW GPU - Animation Project");
        Projects newProject = new Projects();
        newProject.createAnimationProject(driver, host, wait, "bmw27_gpu.blend",
                ComputeOptions.GPU, EngineOptions.CYCLES, false, "3840", "2160",
                true, "0", true, null,
                null, null, true, null, false, true);
    }

    @Test
    public void test_create_teeglasfx_animation_project() throws InterruptedException {
        Utils.commentGenerator("Starting TeeglasFX - Animation Project");
        Projects newProject = new Projects();
        newProject.createAnimationProject(driver, host, wait, "TeeglasFX_27.blend",
                ComputeOptions.CPU, EngineOptions.CYCLES, true, null, null,
                true, null, true, null, null,
                null, true, null, false, true);
    }

    @Test
    public void test_create_helicopter_animation_project() throws InterruptedException {
        Utils.commentGenerator("Starting Helicopter Scene - Animation Project");
        Projects newProject = new Projects();
        newProject.createAnimationProject(driver, host, wait, "scene-Helicopter-27.blend",
                ComputeOptions.CPU_GPU, EngineOptions.CYCLES, false, "2580", "1080",
                false, "500", true, null, null,
                null, true, null, false, true);
    }

    @Test
    public void test_create_volume_emission_animation_project() throws InterruptedException {
        Utils.commentGenerator("Starting Volume Emission - Animation Project - Blender Render");
        Projects newProject = new Projects();
        newProject.createAnimationProject(driver, host, wait, "volume_emission_27.blend",
                ComputeOptions.CPU, EngineOptions.BLENDER_RENDER, false, "1900", "2900",
                true, null, true, null,
                null, null, true, null, false, true);
    }


    @Test
    public void test_create_multiple_random_projects_blender_render() throws InterruptedException {
        Utils.commentGenerator("Starting Random - Still & Animation Projects - Blender Render");
        List<String> projectList = new ArrayList<>();
        projectList.add("bmw27_cpu.blend");
        projectList.add("bmw27_gpu.blend");
        projectList.add("Ring_27.blend");
        projectList.add("TeeglasFX_27.blend");
        projectList.add("scene-Helicopter-27.blend");
        projectList.add("volume_emission_27.blend");
        Projects newProject = new Projects();
        newProject.createMultipleProjects(driver, host, wait, projectList, 10, EngineOptions.BLENDER_RENDER);
    }

    @Test
    public void test_create_multiple_random_projects_cycles() throws InterruptedException {
        Utils.commentGenerator("Starting Random - Still & Animation Projects - Cycles");
        List<String> projectList = new ArrayList<>();
        projectList.add("bmw27_cpu.blend");
        projectList.add("bmw27_gpu.blend");
        projectList.add("Ring_27.blend");
        projectList.add("TeeglasFX_27.blend");
        projectList.add("scene-Helicopter-27.blend");
        projectList.add("volume_emission_27.blend");
        Projects newProject = new Projects();
        newProject.createMultipleProjects(driver, host, wait, projectList, 10, EngineOptions.CYCLES);
    }

}
