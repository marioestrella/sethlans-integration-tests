package com.dryadandnaiad.integration.project;

import com.dryadandnaiad.test.utils.*;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created Mario Estrella on 1/8/18.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class ProjectRenderTestsDual extends AbstractProjectCreationTest {

    @BeforeClass
    public static void dualSetup() throws InterruptedException {
        DualSetup dualSetup = new DualSetup();
        dualSetup.startSetup(wait, driver, setupHost, sethlansOS, usernameString, passwordString, validPort, ComputeOptions.CPU);
    }

    @Test
    public void test_create_bmw_cpu_still_project() throws InterruptedException {
        Thread.sleep(2000);
        WebElement menuButton = driver.findElement(By.id("settings"));
        menuButton.click();
        Utils.commentGenerator("Loading Settings");
        Thread.sleep(2000);
        WebElement computeSettings = driver.findElement(By.id("settings_compute"));
        computeSettings.click();
        Thread.sleep(1000);
        WebElement cpuGroup = driver.findElement(By.id("cpu"));
        Utils.selectMaxSliderValue(driver, cpuGroup);
        WebElement updateButton = driver.findElement(By.id("update_method"));
        updateButton.click();
        Thread.sleep(20000);
        Login login = new Login();
        login.sethlansLogin(usernameString, passwordString, wait, driver);
        Thread.sleep(2000);
        menuButton = driver.findElement(By.id("settings"));
        menuButton.click();
        Utils.commentGenerator("Loading Settings");
        Thread.sleep(2000);
        DualBenchmarkNode cpuDualBenchmarkNode = new DualBenchmarkNode();
        cpuDualBenchmarkNode.startBenchmark(driver, sethlansOS, validPort, host, ComputeOptions.CPU, wait);

        Utils.commentGenerator("Starting BMW CPU - Still Project Render");
        Projects newProject = new Projects();
        newProject.createStillImageProject(driver, host, wait, "bmw27_cpu.blend",
                ComputeOptions.CPU, EngineOptions.CYCLES, false, "1920", "1080",
                false, "5",
                true, false);
        Thread.sleep(2000);
        driver.get(host + "project");
        Thread.sleep(1000);
        RenderProject renderProject = new RenderProject();
        renderProject.startRender(driver, 1);

    }

}
