package com.dryadandnaiad.integration.server_node.render;

import com.dryadandnaiad.integration.server_node.AbstractServerNodeTests;
import com.dryadandnaiad.test.utils.*;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created Mario Estrella on 1/11/18.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration
 */
public class RenderStressTestServerNodeCPU extends AbstractServerNodeTests {

    @BeforeClass
    public static void addNodesandBenchmark() throws InterruptedException {
        addNodeandAcknowledge();
        verifyBenchmarks();
        serverDriver.quit();
    }


    @Test
    public void renderTestsAnimationLargeMaxParts() throws InterruptedException {
        Utils.commentGenerator("Starting Pavillon - Animation Project Render - 2400 parts");
        Projects newProject = new Projects();
        Integer projectId = newProject.createAnimationProject(serverDriver, Utils.getHostURL(server, validPort), serverWait,
                "pavillon_barcelone_v1.2_textures_animation.blend",
                ComputeOptions.CPU, EngineOptions.CYCLES, true, null, null,
                false, "1", false, 1, 100, 1,
                false, RenderOutputFormat.PNG, true, false);
        Thread.sleep(2000);
        serverDriver.get(Utils.getHostURL(server, validPort) + "project");
        Thread.sleep(1000);
        RenderProject renderProject = new RenderProject();
        renderProject.startRender(serverDriver, projectId);
    }
}
