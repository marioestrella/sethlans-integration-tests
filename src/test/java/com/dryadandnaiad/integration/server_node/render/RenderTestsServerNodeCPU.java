package com.dryadandnaiad.integration.server_node.render;

import com.dryadandnaiad.integration.server_node.AbstractServerNodeTests;
import com.dryadandnaiad.test.utils.*;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created Mario Estrella on 1/11/18.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration
 */
public class RenderTestsServerNodeCPU extends AbstractServerNodeTests {

    @BeforeClass
    public static void addNodesandBenchmark() throws InterruptedException {
        addNodeandAcknowledge();
        verifyBenchmarks();
        serverDriver.quit();
    }

    @Test
    public void renderTestSingleFrame() throws InterruptedException {
        Utils.commentGenerator("Starting BMW CPU - Still Project Render");
        Projects newProject = new Projects();
        Integer projectId = newProject.createStillImageProject(serverDriver, Utils.getHostURL(server, validPort), serverWait, "bmw27_cpu.blend",
                ComputeOptions.CPU, EngineOptions.CYCLES, false, "1920", "1080",
                false, "5", false, false);
        Thread.sleep(2000);
        serverDriver.get(Utils.getHostURL(server, validPort) + "project");
        Thread.sleep(1000);
        RenderProject renderProject = new RenderProject();
        renderProject.startRender(serverDriver, projectId);
    }


    @Test
    public void renderTestsAnimationSmall() throws InterruptedException {
        Utils.commentGenerator("Starting Pavillon - Animation Project Render - Small");
        Projects newProject = new Projects();
        Integer projectId = newProject.createAnimationProject(serverDriver, Utils.getHostURL(server, validPort), serverWait,
                "pavillon_barcelone_v1.2_textures_animation.blend",
                ComputeOptions.CPU, EngineOptions.CYCLES, true, null, null,
                false, "5", false, 1, 25, 1,
                false, RenderOutputFormat.PNG, false, false);
        Thread.sleep(2000);
        serverDriver.get(Utils.getHostURL(server, validPort) + "project");
        Thread.sleep(1000);
        RenderProject renderProject = new RenderProject();
        renderProject.startRender(serverDriver, projectId);
    }


    @Test
    public void renderTestsAnimationSkip() throws InterruptedException {
        Utils.commentGenerator("Starting Pavillon - Animation Project Render - Skipped Frames");
        Projects newProject = new Projects();
        Integer projectId = newProject.createAnimationProject(serverDriver, Utils.getHostURL(server, validPort), serverWait,
                "pavillon_barcelone_v1.2_textures_animation.blend",
                ComputeOptions.CPU, EngineOptions.CYCLES, true, null, null,
                false, "5", false, 1, 374, 3,
                false, RenderOutputFormat.PNG, false, false);
        Thread.sleep(2000);
        serverDriver.get(Utils.getHostURL(server, validPort) + "project");
        Thread.sleep(1000);
        RenderProject renderProject = new RenderProject();
        renderProject.startRender(serverDriver, projectId);
    }
}
