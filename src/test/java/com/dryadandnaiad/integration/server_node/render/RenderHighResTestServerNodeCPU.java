package com.dryadandnaiad.integration.server_node.render;

import com.dryadandnaiad.integration.server_node.AbstractServerNodeTests;
import com.dryadandnaiad.test.utils.*;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created Mario Estrella on 1/11/18.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration
 */
public class RenderHighResTestServerNodeCPU extends AbstractServerNodeTests {

    @BeforeClass
    public static void addNodesandBenchmark() throws InterruptedException {
        addNodeandAcknowledge();
        verifyBenchmarks();
        serverDriver.quit();
    }

    @Test
    public void renderTestBMWHighRes() throws InterruptedException {
        Utils.commentGenerator("Starting BMW CPU - Still Project Render - High Res");
        Projects newProject = new Projects();
        Integer projectId = newProject.createStillImageProject(serverDriver, Utils.getHostURL(server, validPort), serverWait, "bmw27_cpu.blend",
                ComputeOptions.CPU, EngineOptions.CYCLES, false, "3840", "2160",
                false, "50", true, false);
        Thread.sleep(2000);
        serverDriver.get(Utils.getHostURL(server, validPort) + "project");
        Thread.sleep(1000);
        RenderProject renderProject = new RenderProject();
        renderProject.startRender(serverDriver, projectId);
    }

    @Test
    public void renderTestTeaGlassHighSamples() throws InterruptedException {
        Utils.commentGenerator("Starting Tea Glass - Still Project Render - High Res");
        Projects newProject = new Projects();
        Integer projectId = newProject.createStillImageProject(serverDriver, Utils.getHostURL(server, validPort), serverWait,
                "TeeglasFX_27.blend",
                ComputeOptions.CPU, EngineOptions.CYCLES, false, "1500", "1500",
                false, "1200", true, false);
        Thread.sleep(2000);
        serverDriver.get(Utils.getHostURL(server, validPort) + "project");
        Thread.sleep(1000);
        RenderProject renderProject = new RenderProject();
        renderProject.startRender(serverDriver, projectId);
    }

    @Test
    public void renderTestRingHighSamples() throws InterruptedException {
        Utils.commentGenerator("Starting Ring - Still Project Render - High Res");
        Projects newProject = new Projects();
        Integer projectId = newProject.createStillImageProject(serverDriver, Utils.getHostURL(server, validPort), serverWait,
                "Ring_27.blend",
                ComputeOptions.CPU, EngineOptions.CYCLES, false, "1500", "1500",
                false, "2000", true, false);
        Thread.sleep(2000);
        serverDriver.get(Utils.getHostURL(server, validPort) + "project");
        Thread.sleep(1000);
        RenderProject renderProject = new RenderProject();
        renderProject.startRender(serverDriver, projectId);
    }

    @Test
    public void renderTestHelicopterHighSamples() throws InterruptedException {
        Utils.commentGenerator("Starting Ring - Still Project Render - High Res");
        Projects newProject = new Projects();
        Integer projectId = newProject.createStillImageProject(serverDriver, Utils.getHostURL(server, validPort), serverWait,
                "scene-Helicopter-27.blend",
                ComputeOptions.CPU, EngineOptions.CYCLES, false, "3840", "2160",
                false, "2000", true, false);
        Thread.sleep(2000);
        serverDriver.get(Utils.getHostURL(server, validPort) + "project");
        Thread.sleep(1000);
        RenderProject renderProject = new RenderProject();
        renderProject.startRender(serverDriver, projectId);
    }


}
