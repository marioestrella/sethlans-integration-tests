package com.dryadandnaiad.integration.server_node;

import com.dryadandnaiad.test.utils.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created Mario Estrella on 1/11/18.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration
 */
public class AbstractServerNodeTests {

    protected static WebDriver serverDriver;
    public static String usernameString;
    public static String passwordString;
    public static Integer validPort;
    public static String server = System.getProperty("server").toLowerCase();
    private static String nodes = System.getProperty("nodes").toLowerCase();
    private static List<String> nodeList;
    public static DesiredCapabilities capabilities = new DesiredCapabilities();
    protected static WebDriverWait serverWait;
    private static Integer setupPort = 7443;

    @BeforeClass
    public static void setup() throws InterruptedException {
        nodes = nodes.replaceAll("\\s", "");
        nodeList = Arrays.asList(nodes.split(","));
        Utils.commentGenerator("Server: " + server);
        Utils.commentGenerator("List of Nodes: " + nodeList.toString());
        validPort = Utils.validPortGenerator(7000, 8000);
        usernameString = Utils.usernameGenerator(6);
        passwordString = Utils.passwordGenerator(10) + "VxZy";
        Utils.setDriver();
        capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        Utils.commentGenerator("Username used during test: " + usernameString);
        Utils.commentGenerator("Password used during test: " + passwordString);
        Utils.commentGenerator("Sethlans Port: " + validPort);

        Utils.commentGenerator("Configuring Sethlans Server");
        serverDriver = new FirefoxDriver(capabilities);
        serverWait = new WebDriverWait(serverDriver, 60);
        Thread serverThread = new Thread(() -> {
            ServerSetup serverSetup = new ServerSetup();
            try {
                serverSetup.startSetup(serverWait, serverDriver, Utils.getHostURL(server, setupPort), server, usernameString, passwordString, validPort);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        serverThread.start();


        Utils.commentGenerator("Configuration Sethlans Nodes");
        for (String node : nodeList) {
            Utils.commentGenerator("Starting node setup for " + node);
            WebDriver nodeDriver = new FirefoxDriver(capabilities);
            WebDriverWait nodeWait = new WebDriverWait(nodeDriver, 60);
            Thread nodeThread = new Thread(() -> {
                NodeSetup nodeSetup = new NodeSetup();
                try {
                    nodeSetup.startSetup(nodeWait, nodeDriver, Utils.getHostURL(node, setupPort), node, usernameString, passwordString, validPort, ComputeOptions.CPU);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            });
            nodeThread.start();
            Thread.sleep(4000);
        }

        Utils.commentGenerator("Sleeping for 2 minutes while setup process completes for all systems");
        Thread.sleep(120000);

    }

    @Before
    public void startBrowser() throws InterruptedException {
        capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        serverDriver = new FirefoxDriver(capabilities);
        serverWait = new WebDriverWait(serverDriver, 60);
        Thread.sleep(5000);
        Utils.commentGenerator("Navigating to " + Utils.getHostURL(server, validPort) + "home");
        serverDriver.get(Utils.getHostURL(server, validPort) + "home");
        Login login = new Login();
        login.sethlansLogin(usernameString, passwordString, serverWait, serverDriver);
    }

    protected static void addNodeandAcknowledge() throws InterruptedException {
        serverDriver = new FirefoxDriver(capabilities);
        serverWait = new WebDriverWait(serverDriver, 60);
        Thread.sleep(5000);
        Utils.commentGenerator("Navigating to " + Utils.getHostURL(server, validPort) + "home");
        serverDriver.get(Utils.getHostURL(server, validPort) + "home");
        Login login = new Login();
        login.sethlansLogin(usernameString, passwordString, serverWait, serverDriver);
        Thread.sleep(3000);
        for (String node : nodeList) {
            Utils.commentGenerator("Adding node: " + node + " to server: " + server);
            ServerAddNode addNode = new ServerAddNode();
            addNode.addNode(serverDriver, usernameString, passwordString, serverWait, node, validPort, Utils.getHostURL(server, validPort), ComputeOptions.CPU);
            Thread.sleep(2000);

        }
        Thread.sleep(30000);
        serverDriver.quit();

        List<WebDriver> nodeWebDrivers = new ArrayList<>();
        for (String node : nodeList) {
            Utils.commentGenerator("Starting server acknowledgement " + node);
            WebDriver nodeDriver = new FirefoxDriver(capabilities);
            WebDriverWait nodeWait = new WebDriverWait(nodeDriver, 60);
            nodeWebDrivers.add(nodeDriver);
            Thread nodeThread = new Thread(() -> {
                NodeAckServer nodeAcknowledgement = new NodeAckServer();
                try {
                    nodeAcknowledgement.startServerAcknowledgement(nodeDriver, Utils.getHostURL(node, validPort), validPort,
                            usernameString, passwordString, server.toUpperCase(), ComputeOptions.CPU, nodeWait);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            });
            nodeThread.start();
            Thread.sleep(8000);
        }

        Utils.commentGenerator("Sleeping for 2 1/2 minutes while setup process completes for all systems");
        Thread.sleep(150000);

        for (WebDriver nodeWebDriver : nodeWebDrivers) {
            nodeWebDriver.quit();
        }
    }

    protected static void verifyBenchmarks() throws InterruptedException {
        serverDriver = new FirefoxDriver(capabilities);
        serverWait = new WebDriverWait(serverDriver, 60);
        Thread.sleep(5000);
        Utils.commentGenerator("Navigating to " + Utils.getHostURL(server, validPort) + "home");
        serverDriver.get(Utils.getHostURL(server, validPort) + "home");
        Login login = new Login();
        login.sethlansLogin(usernameString, passwordString, serverWait, serverDriver);
        Thread.sleep(3000);
        ServerVerifyBenchmarks verifyBenchmarks = new ServerVerifyBenchmarks();
        verifyBenchmarks.verifyBenchmarks(serverDriver, nodeList, ComputeOptions.CPU, serverWait);
    }

    @After
    public void logout() {
        Utils.commentGenerator("Current test completed, logging out and quitting webdriver");
        serverDriver.get(Utils.getHostURL(server, validPort) + "logout");
        serverDriver.quit();
    }

    @AfterClass
    public static void teardown() throws InterruptedException {
        Utils.commentGenerator("Benchmark Test Completed");
        Utils.setDriver();
        capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        serverDriver = new FirefoxDriver(capabilities);
        String host = Utils.getHostURL(server, validPort) + "shutdown";
        serverWait = new WebDriverWait(serverDriver, 60);
        Utils.commentGenerator("Shutting Down Sethlans");
        Utils.cleanup(host, serverDriver, serverWait, usernameString, passwordString);
        for (String node : nodeList) {
            String nodeHost = Utils.getHostURL(node, validPort) + "shutdown";
            WebDriver nodeDriver = new FirefoxDriver(capabilities);
            WebDriverWait nodeWait = new WebDriverWait(nodeDriver, 60);
            Thread nodeThread = new Thread(() -> {
                Utils.cleanup(nodeHost, nodeDriver, nodeWait, usernameString, passwordString);


            });
            nodeThread.start();
        }
        Thread.sleep(15000);
    }
}
