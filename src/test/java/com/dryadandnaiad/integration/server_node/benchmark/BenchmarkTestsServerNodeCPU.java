package com.dryadandnaiad.integration.server_node.benchmark;

import com.dryadandnaiad.integration.server_node.AbstractServerNodeTests;
import org.junit.Test;

/**
 * Created Mario Estrella on 12/23/17.
 * Dryad and Naiad Software LLC
 * mestrella@dryadandnaiad.com
 * Project: sethlans-integration-tests
 */
public class BenchmarkTestsServerNodeCPU extends AbstractServerNodeTests {

    @Test
    public void benchmarkNodes() throws InterruptedException {
        addNodeandAcknowledge();
        verifyBenchmarks();

    }


}
