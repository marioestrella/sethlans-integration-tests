stage('Project Render Tests - Dual') {

    String current_test = "test -Dtest=ProjectRenderTestsDual"
    env.STAGENAME = "project_render_dual"
    parallel linux: {
        node('linux && gpu') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def node_startup = load "${rootDir}/scripts/node_startup.groovy"
            node_startup.linux_startup_script()
        }
        node('selenium') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def selenium = load "${rootDir}/scripts/selenium_scripts.groovy"
            selenium.selenium_linux(current_test)
        }
        node(env.LINUXNODE) {
            def rootDir = pwd()
            def logs = load "${rootDir}/scripts/logs.groovy"
            def node_shutdown = load "${rootDir}/scripts/node_shutdown.groovy"
            logs.get_logs_linux("render")
            node_shutdown.linux_shutdown_script()
        }
    }, windows: {
        node('windows') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def node_startup = load "${rootDir}/scripts/node_startup.groovy"
            node_startup.windows_startup_script()

        }
        node('selenium') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def selenium = load "${rootDir}/scripts/selenium_scripts.groovy"
            selenium.selenium_windows(current_test)
        }
        node(env.WINNODE) {
            def rootDir = pwd()
            def logs = load "${rootDir}/scripts/logs.groovy"
            def node_shutdown = load "${rootDir}/scripts/node_shutdown.groovy"
            logs.get_logs_windows("render")
            node_shutdown.windows_shutdown_script()
        }
    }, mac: {
        node('mac && gpu') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def node_startup = load "${rootDir}/scripts/node_startup.groovy"
            node_startup.mac_startup_script()
        }
        node('selenium') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def selenium = load "${rootDir}/scripts/selenium_scripts.groovy"
            selenium.selenium_mac(current_test)
        }
        node(env.MACNODE) {
            def rootDir = pwd()
            def logs = load "${rootDir}/scripts/logs.groovy"
            def node_shutdown = load "${rootDir}/scripts/node_shutdown.groovy"
            logs.get_logs_mac("render")
            node_shutdown.mac_shutdown_script()
        }
    }, failFast: false
}