stage('Render Stress Test Server and Nodes - CPU') {
    String current_test = "test -Dtest=RenderStressTestServerNodeCPU"
    env.STAGENAME = "render_stress_test_server_node_cpu"

    parallel server: {
        node('server && linux') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def node_startup = load "${rootDir}/scripts/node_startup.groovy"
            def SERVERHOSTNAME = node_startup.linux_startup_script()

            script {
                env.SERVERNODE = env.NODE_NAME
                env.SERVERHOST = SERVERHOSTNAME
            }
        }
    }, node4: {
        node('node && mac2') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def node_startup = load "${rootDir}/scripts/node_startup.groovy"
            def NODE4HOSTNAME = node_startup.mac_startup_script()
            script {
                env.NODENODE4 = env.NODE_NAME
                env.NODEHOST4 = NODE4HOSTNAME
            }

        }

    }, node5: {
        node('node && linux4') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def node_startup = load "${rootDir}/scripts/node_startup.groovy"
            def NODE5HOSTNAME = node_startup.linux_startup_script()
            script {
                env.NODENODE5 = env.NODE_NAME
                env.NODEHOST5 = NODE5HOSTNAME
            }

        }
    } , node1: {
        node('node && windows1') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def node_startup = load "${rootDir}/scripts/node_startup.groovy"
            def NODE1HOSTNAME = node_startup.windows_startup_script()
            script {
                env.NODENODE1 = env.NODE_NAME
                env.NODEHOST1 = NODE1HOSTNAME
            }

        }
    }, node2: {
        node('node && mac1') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def node_startup = load "${rootDir}/scripts/node_startup.groovy"
            def NODE2HOSTNAME = node_startup.mac_startup_script()
            script {
                env.NODENODE2 = env.NODE_NAME
                env.NODEHOST2 = NODE2HOSTNAME
            }

        }
    }, node3: {
        node('node && linux1') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def node_startup = load "${rootDir}/scripts/node_startup.groovy"
            def NODE3HOSTNAME = node_startup.linux_startup_script()
            script {
                env.NODENODE3 = env.NODE_NAME
                env.NODEHOST3 = NODE3HOSTNAME
            }
        }
    } , node6: {
        node('node && mac4') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def node_startup = load "${rootDir}/scripts/node_startup.groovy"
            def NODE6HOSTNAME = node_startup.mac_startup_script()
            script {
                env.NODENODE6 = env.NODE_NAME
                env.NODEHOST6 = NODE6HOSTNAME
            }

        }
    }, node7: {
        node('node && linux2') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def node_startup = load "${rootDir}/scripts/node_startup.groovy"
            def NODE7HOSTNAME =  node_startup.linux_startup_script()
            script {
                env.NODENODE7 = env.NODE_NAME
                env.NODEHOST7 = NODE7HOSTNAME
            }
        }
    }, node8: {
        node('node && mac3') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def node_startup = load "${rootDir}/scripts/node_startup.groovy"
            def NODE8HOSTNAME = node_startup.mac_startup_script()
            script {
                env.NODENODE8 = env.NODE_NAME
                env.NODEHOST8 = NODE8HOSTNAME
            }

        }
    }, node9: {
        node('node && linux3') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def node_startup = load "${rootDir}/scripts/node_startup.groovy"
            def NODE9HOSTNAME = node_startup.linux_startup_script()
            script {
                env.NODENODE9 = env.NODE_NAME
                env.NODEHOST9 = NODE9HOSTNAME
            }
        }
    }


    node('selenium && dedicated') {
        sh "rm -rf *"
        try {
            checkout scm
        } catch (error) {
            emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                    replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                    to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                            [$class: 'RequesterRecipientProvider']]))
        }
        def rootDir = pwd()
        def selenium = load "${rootDir}/scripts/selenium_scripts.groovy"
        selenium.selenium_multi_2(current_test)
    }

    parallel shutdown_server: {
        node(env.SERVERNODE) {
            def rootDir = pwd()
            def logs = load "${rootDir}/scripts/logs.groovy"
            def node_shutdown = load "${rootDir}/scripts/node_shutdown.groovy"
            logs.get_archives_multi('$SERVERNODE','server render')
            node_shutdown.linux_shutdown_script()
        }
    }, shutdown_node1: {
        node(env.NODENODE1) {
            def rootDir = pwd()
            def logs = load "${rootDir}/scripts/logs.groovy"
            def node_shutdown = load "${rootDir}/scripts/node_shutdown.groovy"
            logs.get_archives_multi('$NODENODE1', 'node')
            node_shutdown.windows_shutdown_script()
        }
    }, shutdown_node2: {
        node(env.NODENODE2) {
            def rootDir = pwd()
            def logs = load "${rootDir}/scripts/logs.groovy"
            def node_shutdown = load "${rootDir}/scripts/node_shutdown.groovy"
            logs.get_archives_multi('$NODENODE2', 'node')
            node_shutdown.mac_shutdown_script()
        }
    }, shutdown_node3: {
        node(env.NODENODE3) {
            def rootDir = pwd()
            def logs = load "${rootDir}/scripts/logs.groovy"
            def node_shutdown = load "${rootDir}/scripts/node_shutdown.groovy"
            logs.get_archives_multi('$NODENODE3', 'node')
            node_shutdown.linux_shutdown_script()
        }
    }, shutdown_node4: {
        node(env.NODENODE4) {
            def rootDir = pwd()
            def logs = load "${rootDir}/scripts/logs.groovy"
            def node_shutdown = load "${rootDir}/scripts/node_shutdown.groovy"
            logs.get_archives_multi('$NODENODE4', 'node')
            node_shutdown.linux_shutdown_script()
        }
    }, shutdown_node5: {
        node(env.NODENODE5) {
            def rootDir = pwd()
            def logs = load "${rootDir}/scripts/logs.groovy"
            def node_shutdown = load "${rootDir}/scripts/node_shutdown.groovy"
            logs.get_archives_multi('$NODENODE5', 'node')
            node_shutdown.windows_shutdown_script()
        }
    } , shutdown_node6: {
        node(env.NODENODE6) {
            def rootDir = pwd()
            def logs = load "${rootDir}/scripts/logs.groovy"
            def node_shutdown = load "${rootDir}/scripts/node_shutdown.groovy"
            logs.get_archives_multi('$NODENODE6', 'node')
            node_shutdown.mac_shutdown_script()
        }
    }, shutdown_node7: {
        node(env.NODENODE7) {
            def rootDir = pwd()
            def logs = load "${rootDir}/scripts/logs.groovy"
            def node_shutdown = load "${rootDir}/scripts/node_shutdown.groovy"
            logs.get_archives_multi('$NODENODE7', 'node')
            node_shutdown.linux_shutdown_script()
        }
    }, shutdown_node8: {
        node(env.NODENODE8) {
            def rootDir = pwd()
            def logs = load "${rootDir}/scripts/logs.groovy"
            def node_shutdown = load "${rootDir}/scripts/node_shutdown.groovy"
            logs.get_archives_multi('$NODENODE8', 'node')
            node_shutdown.mac_shutdown_script()
        }
    }, shutdown_node9: {
        node(env.NODENODE9) {
            def rootDir = pwd()
            def logs = load "${rootDir}/scripts/logs.groovy"
            def node_shutdown = load "${rootDir}/scripts/node_shutdown.groovy"
            logs.get_archives_multi('$NODENODE9', 'node')
            node_shutdown.linux_shutdown_script()
        }
    }


}
