stage('Setup Wizard Tests') {
    env.STAGENAME = "setup_wizard_tests"
    parallel linux: {
        node('linux') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def node_startup = load "${rootDir}/scripts/node_startup.groovy"
            node_startup.linux_startup_script()
        }
        node('selenium') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def selenium = load "${rootDir}/scripts/selenium_scripts.groovy"
            selenium.selenium_linux("test")
        }
        node(env.LINUXNODE) {
            def rootDir = pwd()
            def logs = load "${rootDir}/scripts/logs.groovy"
            def node_shutdown = load "${rootDir}/scripts/node_shutdown.groovy"
            logs.get_logs_linux(" ")
            node_shutdown.linux_shutdown_script()
        }
    }, windows: {
        node('windows') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def node_startup = load "${rootDir}/scripts/node_startup.groovy"
            node_startup.windows_startup_script()
        }
        node('selenium') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def selenium = load "${rootDir}/scripts/selenium_scripts.groovy"
            selenium.selenium_windows("test")
        }
        node(env.WINNODE) {
            def rootDir = pwd()
            def logs = load "${rootDir}/scripts/logs.groovy"
            def node_shutdown = load "${rootDir}/scripts/node_shutdown.groovy"
            logs.get_logs_windows(" ")
            node_shutdown.windows_shutdown_script()
        }
    }, mac: {
        node('mac') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def node_startup = load "${rootDir}/scripts/node_startup.groovy"
            node_startup.mac_startup_script()
        }
        node('selenium') {
            sh "rm -rf *"
            try {
                checkout scm
            } catch (error) {
                emailext(body: '${DEFAULT_CONTENT}', mimeType: 'text/html',
                        replyTo: '$DEFAULT_REPLYTO', subject: '${DEFAULT_SUBJECT}',
                        to: emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                [$class: 'RequesterRecipientProvider']]))
            }
            def rootDir = pwd()
            def selenium = load "${rootDir}/scripts/selenium_scripts.groovy"
            selenium.selenium_mac("test")

        }
        node(env.MACNODE) {
            def rootDir = pwd()
            def logs = load "${rootDir}/scripts/logs.groovy"
            def node_shutdown = load "${rootDir}/scripts/node_shutdown.groovy"
            logs.get_logs_mac(" ")
            node_shutdown.mac_shutdown_script()
        }
    }
}
